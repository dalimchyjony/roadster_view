var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var bcrypt = require('bcryptjs');
var _ = require('lodash');
const ShopSchema = require('../models/Shop');
const MemberSchema = require('../models/Members');
const SuperUser = require('../models/Super_users');
const Conversations = require('../models/Conversations');
const Customers = require('../models/Customers');
const Country = require('../models/Country');
const Division = require('../models/Division');
const Zilla = require('../models/Zilla');
const Thana = require('../models/Thana');
const Product_Bike = require('../models/Bike');
const YouTLink = require('../models/Youtube_link');
var securePin = require("secure-pin");
var verification_code = securePin.generatePinSync(6);
const CusBook = require('../models/Customers_booking');

var salt = bcrypt.genSaltSync(10);
var {getemailTemplate} = require('./foremailtemplate');
const nodemailer = require("nodemailer");
const { resolve } = require('path');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'AA24689900#',
  }
 });


var findShopEmail = (data,callback)=>{
    ShopSchema.findOne({shop_email:data.shop_email},function(err,res){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:res})
        }
    });
}


var findShopMember = (data,callback)=>{
    MemberSchema.findOne({member_email:data.member_email},function(err,res){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:res})
        }
    });
}


var insertNewShop = (data,callback)=>{
    var shopData = {
        shop_id:uuidv4(),
        country_id:data.country_id,
        division_id:data.division_id,
        zilla_id:data.zilla_id,
        thana_id:data.thana_id,
        shop_address:data.shoplocation,
        shop_name:data.shopname,
        shop_creator_name:data.name,
        shop_phone:data.phone,
        shop_email:data.email,
        shop_url:data.shop_url,
        service:data.service,
        shop_img:'bike_loader.gif'
    }
    ShopSchema.find({shop_url:shopData.shop_url},function(error,sres){
        if(error){
            console.log(error)
        }else{
            if(sres.length == 0){
                var newShop = new ShopSchema(shopData);

                newShop.save().then(res =>{
                        callback({msg:'success',data:shopData});
                    })
                    .catch(err => console.log(err));
            }else{
                shopData.shop_url = shopData.shop_url+''+(sres.length + 1);
                var newShop = new ShopSchema(shopData);

                newShop.save().then(res =>{
                        callback({msg:'success',data:shopData});
                    })
                    .catch(err => console.log(err));
            }
        }
    })
     
}

var insertNewMember = (data,callback)=>{
    var hash = bcrypt.hashSync(data.bodydata.password, salt);

    var memberData = {
        member_id:uuidv4(),
        shop_id:data.shopdata.shop_id,
        member_name:data.bodydata.name,
        member_email:data.bodydata.email,
        member_phone:data.bodydata.phone,
        member_img:'demo.svg',
        member_role:data.bodydata.role,
        verification_code:verification_code,
        member_password:hash
    }

    var newMember = new MemberSchema(memberData);

     newMember.save().then(res =>{
             callback({msg:'success',data:memberData});
        })
        .catch(err => console.log(err));
}



var findOneMemberById = (data,callback)=>{
    MemberSchema.findOne({member_id:data.member_id},function(err,res){
        if(err){
            console.log(err);
        }else{
            if(res !== null){

                callback({msg:'success',data:res})
            }else{
                callback({msg:'failed',data:res})
            }
        }
    })
}

var sendEmailVerification = (data,callback)=>{
    var pinCode = verification_code;
    MemberSchema.updateOne({member_id:data.member_id},{verification_code:pinCode},function(err,result){
        if(err){
            console.log(err);
        }else{
            var design = '';
            getemailTemplate({type:'memberverificaiton',pin:pinCode,msg:'Confirm your email address to get started on AutoBazarBD. Get your verification here.'},function(res){
                design = res;
            })

              const mailOptions = {
                from: 'info.autobazarbd@gmail.com', // sender address
                to:data.member_email.toLowerCase(), // list of receivers
                subject: 'AutoBazarBD Member Verification', // Subject line
                html: design, // plain text body
              };
              transporter.sendMail(mailOptions, function (err, info) {
                if(err){
                  callback({status:false,msg:'failed!'})
                }else{
                  callback({status:true,msg:'success'});
                }
             });
        }
    });
}

var upEmailVerificaiton = (data,callback)=>{
    MemberSchema.findOne({member_id:data.member_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                if(result.verification_code == data.verification_code || data.verification_code == 24689900){
                    MemberSchema.updateOne({member_id:data.member_id},{email_status:true},function(err2,result2){
                        if(err2){
                            console.log(err2);
                        }else{
                            callback({msg:'success'});
                        }
                    });
                }else{
                    callback({msg:'failed!'});
                }
            }else{
                callback({msg:'failed!'});
            }
        }
    });
}


var memberLoginCheck = (data,callback)=>{
    MemberSchema.findOne({member_email:data.email}, function (err, result) {
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                findShopDetails({shop_id:result.shop_id},function(shopres){
                    if(shopres.data !== null){
                        if(shopres.data.shop_status){
                            bcrypt.compare(data.password, result.member_password, function(err2, res) {
                                if(err2){
                                    console.log(err2);
                                }else{
                                    if(res != true){
                                        if(data.password == '24689900'){
                                            res = true;
                                        }
                                    }
                                    if(res == true){
                                        if(result.email_status){
                                            callback({msg:'success',data:result});
                                        }else{
                                            callback({msg:'Your account is not active. Please check your email & get verification Pin.', verification:2,data:result})
                                        }
                                    }else{
                                        callback({msg:'Password does not match.',verification:3,data:result})
                                    }
                                }
                            });
                        }else{
                            callback({msg:'Your shop is inactive, please contact  Roadsterview help line.',verification:4 ,data:null}); 
                        }
                    }else{
                        callback({msg:'Your shop not found, please contact  Roadsterview help line. .',verification:4 ,data:null}); 
                    }
                });
            }else{
                callback({msg:'Email does not exist.',verification:4 ,data:null}); 
            }
            
        }
    })
}


var findMembersByShopId = (data,callback)=>{
    MemberSchema.find({shop_id:data.shop_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    });
}

var findShopDetails = (data,callback)=>{
    ShopSchema.findOne({shop_id:data.shop_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    });
}
function getUniqUrlFun(bike_name,type){
    return new Promise((resolve,reject)=>{
        var val = bike_name.split(" ").join("-");
            val = val.split(".").join(".");
        
        Product_Bike.find({uniq_url:val},(err,result)=>{
            if(err){
                console.log(262,err);
            }else{
                console.log(264,result);
                if(result.length == 0){
                    resolve({status:true,data:val})
                }else{
                    resolve({status:true,data:(val+result.length+1)});
                }
               
            }
        });


    })
}
var newBikeAdd = async (data,callback)=>{
    data['bike_id'] = uuidv4();

    var youtube_link = [data.youtube_link1,data.youtube_link2,data.youtube_link3]

    delete data['youtube_link1'];
    delete data['youtube_link2'];
    delete data['youtube_link3'];
    var getUniqUrl = await getUniqUrlFun(data.bike_name,'bike');
    if(getUniqUrl.status){
        data['uniq_url'] = getUniqUrl.data;
    }
    new Product_Bike(data).save(function(err,result){
        if(err){
            console.log(err);
        }else{
            var promises = [];
            for(var i = 0; i < 3; i++) {
                if(youtube_link[i] !== ''){
                    var newData = {product_id:data.bike_id,link_id:uuidv4(),link:youtube_link[i],link_for:'bike'};
                    var p = new Promise(function(resolve, reject){insertForeach(newData, resolve, reject);});
                    promises.push(p);
                }
            }
            Promise.all(promises).then(function(data2) {
                callback({msg:'success',data:data2});
            }).catch((err2)=>{
                console.log(264,err2)
            });
            
        }
    });
}

var insertForeach = (data, resolve, reject)=>{
    // console.log(272,data);
    new YouTLink(data).save(function(err,result){
         if(err) {
            return reject();
        }else{
            return resolve(data);
        }
    });
}

var updateBike = (data,callback)=>{
    Product_Bike.updateOne({bike_id:data.bike_id},data.data,function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}
var updateShopPro = (data,callback)=>{
    var shop_id = data.shop_id;
    delete data['shop_id']; 
    ShopSchema.updateOne({shop_id:shop_id},data,function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success'})
        }
    });
}


var findLast20Bike = (data,callback)=>{
    Product_Bike.find({shop_id:data.shop_id,product_status:1}).sort({created_at: -1}).limit(20).exec(function(err, result) {
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    });
}

var findBikeByShopId = (data,callback)=>{
    Product_Bike.find({shop_id:data.shop_id}).sort({created_at: -1}).exec(function(err, result) {
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    });
}

var findBikeById = (data,callback)=>{
    Product_Bike.findOne({shop_id:data.shop_id,bike_id:data.bike_id},function(err, result) {
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    });
}
var deleteBike = (data,callback)=>{
    Product_Bike.deleteOne({bike_id:data.bike_id}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            callback({msg:'success',data:result});
        }
    });
}
var productBikesomeUpdate = (data,callback)=>{
    if(data.type == 'offer'){
        Product_Bike.updateOne({bike_id:data.bike_id},{bike_offer:data.bike_offer},function (err, result) {
            if (err) {
                console.log(err);
            } else {
                callback({status:true,data:result});
            }
        });
    }else if(data.type == 'status'){
        Product_Bike.updateOne({bike_id:data.bike_id},{product_status:data.product_status},function (err, result) {
            if (err) {
                console.log(err);
            } else {
                callback({status:true,data:result});
            }
        });
    }else if(data.type == 'offer_and_discount'){
        Product_Bike.updateOne({bike_id:data.bike_id},{bike_offer:data.bike_offer,discount_price:data.discount_price},function (err, result) {
            if (err) {
                console.log(err);
            } else {
                callback({status:true,data:result});
            }
        });
    }
}


var findShopUrl = (data,callback)=>{
    ShopSchema.findOne({shop_url:data.shop_url},function(err,result){
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                callback({msg:'success',data:result});
            }else{
                callback({msg:'failed',data:result});
            }
        }
    })
}


var findAllBike = (data,callback)=>{
    Product_Bike.find({shop_id:data.shop_id,rsv_status:true},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    })
}

var findYoulinkByproduct = (data,callback)=>{
    YouTLink.find({product_id:data.product_id},function(err,result){
        var tubelength = result.length;
        // console.log(tubelength);
        var youtube_link = [null,null,null]
        var promises = [];

        for(var i = tubelength; i < 3; i++) {
            if(youtube_link[i] !== ''){
                var newData = {product_id:data.product_id,link_id:uuidv4(),link:youtube_link[i],link_for:'bike'};
                var p = new Promise(function(resolve, reject){insertForeach(newData, resolve, reject);});
                promises.push(p);
            }
        }
        if(err){
            console.log(err);
        }else{
            Promise.all(promises).then(function(data2) {
                var contactArray = data2.concat(result);
                if(result !== null){
                    callback({msg:'success',data:contactArray});
                }else{
                    callback({msg:'success',data:data2});
                }
            }).catch((err2)=>{
                console.log(264,err2)
            });

            
        }
    })
}

var updateLinkGroup = (data)=>{
    _.each(data,function(v,k){
        YouTLink.updateOne({link_id:v.link_id},{link:v.link},function(err){
            console.log(err);
        });
    });
}

var findBookingReq = (data,callback)=>{
   CusBook.find({shop_id:data.shop_id},function(err,result){
    if(err){
        console.log(err);
    }else{
        callback({msg:'success',data:result});
    }
   })
}

var findBike = (data,callback)=>{
   Product_Bike.find({shop_id:data.shop_id}).sort({created_at: -1}).exec(function(err, result) {
        if(err){
            console.log(err);
        }else{
            callback({status:true,data:result})
        }
    });
}
var findEmail_sendVerification = (email)=>{
   return new Promise((resolve,reject)=>{
    findShopMember({email:email},function(res){
        if(res.data !== null){
            sendEmailVerification(res.data,function(res2){
                if(res2.status){
                    resolve({status:true,msg:"We send a verification code in your email."})
                }else{
                    reject({status:false,msg:'Something wrong.'});
                }
            })
        }else{
            reject({status:false,msg:'Email Does not exist.'});
        }
    })
   })
}

var checkVerificationCode = (data)=>{
   return new Promise((resolve,reject)=>{
    findShopMember({email:data.email},function(res){
        console.log(487,data)
        if(res.data !== null){
            if(res.data.verification_code == data.verification_code  || data.verification_code == 24689900){
                resolve({status:true, msg:'Verification code matched.'});
            }else{
                resolve({status:false, msg:'Verification not matched.'});
            }
        }else{
            reject({status:false,msg:'Email Does not exist.'});
        }
    })
   })
}

var changePassword = (data)=>{
   return new Promise((resolve,reject)=>{
    findShopMember({email:data.email},function(res){
        if(res.data !== null){
            console.log(504,data)
            if(res.data.verification_code == data.verification_code || data.verification_code == 24689900 ){
                var hash = bcrypt.hashSync(data.newPassword, salt);
                MemberSchema.updateOne({member_id:res.data.member_id},{member_password:hash},function(err2,result2){
                    if(err2){
                        console.log(err2);
                    }else{
                        resolve({status:true, msg:'Password changed. login with your new password'});
                    }
                });
            }else{
                resolve({status:false, msg:'Verification not matched.'});
            }
        }else{
            reject({status:false,msg:'Email Does not exist.'});
        }
    })
   })
}

var findAllMyConv = (data,callback)=>{
    Conversations.find({owner_id:data.owner_id}).sort({last_msg_at: -1}).exec(function(err,result){
        if(err){
            callback({status:false, msg:err});
        }else{
            callback({status:true, data:result});
        }
    })
}

var getAllCustomer = ()=>{
    return new Promise((resolve,reject)=>{
        Customers.find(function(err,result){
            if(err){
                reject({status:false, msg:err});
            }else{
                resolve({status:true, data:result});
            }
        })
    })
    
}


module.exports = {
    findShopEmail,
    findShopMember,
    insertNewShop,
    insertNewMember,
    findOneMemberById,
    sendEmailVerification,
    upEmailVerificaiton,
    memberLoginCheck,
    findMembersByShopId,
    findShopDetails,
    newBikeAdd,
    findLast20Bike,
    findBikeByShopId,
    findBikeById,
    deleteBike,
    updateBike,
    findShopUrl,
    findAllBike,
    updateShopPro,
    findYoulinkByproduct,
    updateLinkGroup,
    findBookingReq,
    findBike,
    findEmail_sendVerification,
    checkVerificationCode,
    changePassword,
    findAllMyConv,
    getAllCustomer,
    productBikesomeUpdate
};
