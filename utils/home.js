var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var bcrypt = require('bcryptjs');
var _ = require('lodash');
const ShopSchema = require('../models/Shop');
const MemberSchema = require('../models/Members');
const SuperUser = require('../models/Super_users');
const Country = require('../models/Country');
const Division = require('../models/Division');
const Zilla = require('../models/Zilla');
const Thana = require('../models/Thana');
const BrandShema = require('../models/Product_brand');
const Product_Bike = require('../models/Bike');
var securePin = require("secure-pin");
var verification_code = securePin.generatePinSync(6);
const ShopShema = require('../models/Shop');
var moment = require('moment');

var salt = bcrypt.genSaltSync(10);

const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'info.autobazarbd24689900',
  }
 });

var cacheBikedata = {
    data:[],
    time:0
};

var findBike = (data,callback)=>{
    Product_Bike.find({product_status:1,rsv_status:true}).sort({created_at:-1}).limit(data.limit).exec(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
    
}

async function countindexNeed(callback){
    var totalBike = await countTotalBike();
    var totalBrand = await countTotalBrand();
    var totalShop = await countTotalShop();

    var data = {
        totalBike:totalBike,
        totalBrand:totalBrand,
        totalShop:totalShop
    }
    callback({status:true,data:data});

}


function countTotalBike(){
    return new Promise((resolve,reject)=>{
        Product_Bike.count({rsv_status:true,product_status:1},function(err,result){
            if(err){
                reject(err);
            }else{
                resolve(result);
            }
        })
    })
}

function countTotalBrand(){
    return new Promise((resolve,reject)=>{
        BrandShema.count({status:true},function(err,result){
            if(err){
                reject(err);
            }else{
                resolve(result);
            }
        })
    })
}

function countTotalShop(){
    return new Promise((resolve,reject)=>{
        ShopSchema.count({shop_status:true},function(err,result){
            if(err){
                reject(err);
            }else{
                resolve(result);
            }
        })
    })
}
var findSingleBike = (data,callback)=>{
    var dataCheck = {bike_id: data.bike_id,product_status:1,rsv_status:true}
    if(data.bike_id == undefined){
        dataCheck = {uniq_url: data.uniq_url,product_status:1,rsv_status:true}
    }
    Product_Bike.findOne(dataCheck,function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}

var findAllDataShop = (callback)=>{
    Country.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            Division.find(function(err1,result1){
                if(err1){
                    console.log(err1);
                }else{
                    Zilla.find(function(err2,result2){
                        if(err2){
                            console.log(err2)
                        }else{
                            Thana.find(function(err3,result3){
                                if(err3){
                                    console.log(err3)
                                }else{
                                    ShopSchema.find({shop_status:true},function(err4,result4){
                                        if(err4){
                                            console.log(err4)
                                        }else{
                                            callback({msg:'success',countrys:result,divisions:result1,zillas:result2,thanas:result3,shops:result4});
                                        }
                                    }); 
                                }
                            });
                        }
                    });
                }
            }); 
        }
    });
}

var homeBikeFilter = (data,callback)=>{
    const bike_nameLike = new RegExp(data.bike_name, 'i');
    data['bike_name'] = bike_nameLike;
    data['product_status'] = 1;
    data['rsv_status'] = true;
    // data['bike_price'] = {$gte :  0, $lte:60000};
    Product_Bike.find(data,function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var homeBikeFilterOnlyRange = (data,callback)=>{ 
    Product_Bike.find({bike_price:{$gte :  data.price_range[0], $lte:data.price_range[1]}},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var findOtherBike = (data,callback)=>{
    Product_Bike.find({shop_id:data.shop_id,rsv_status:true,product_status:1}).limit(50).exec(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var findAllActiveShop = (callback)=>{
    ShopShema.find({shop_status:true},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var filterShopPage = (data,callback)=>{
    if(data.division_id == '' || data.division_id == null){
        delete data.division_id;
    }
    if(data.zilla_id == '' || data.zilla_id == null){
        delete data.zilla_id;
    }
    if(data.thana_id == '' || data.thana_id == null){
        delete data.thana_id;
    }

    data['shop_status'] = true;
    if(data.keyword == ''  || data.keyword == null){
        delete data.keyword;
    }else{

        var shopNamelike = new RegExp(data.keyword, 'i');
        data['shop_name'] = shopNamelike;
        delete data.keyword ;
    }

    ShopShema.find(data,function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({status:true,data:result});
        }
    })
}

var filterProducts = (data, callback)=>{
    // console.log(218, data);
    if(data.brand_id == '' || data.brand_id == null){
        delete data.brand_id;
    }
    if(data.bike_condition == '' || data.bike_condition == null){
        delete data.bike_condition;
    }
    if(data.shop_id == undefined || data.shop_id == '' || data.shop_id == null){
        delete data.shop_id;
    }
    // console.log(226, data);
    Product_Bike.find(data, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            // console.log(231, result);
            callback({status:true,data:result});
        }
    })
}

var searchProductByName = (data, callback)=>{
    var bikeNamelike = new RegExp(data.value, 'i');
    data['bike_name'] = bikeNamelike;
    delete data.value ;
    if(data.shop_id == undefined || data.shop_id == '' || data.shop_id == null){
        delete data.shop_id;
    }
    Product_Bike.find(data, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            callback({status:true,data:result});
        }
    })
}

module.exports = {
    findBike,
    findAllDataShop,
    findSingleBike,
    homeBikeFilter,
    homeBikeFilterOnlyRange,
    findAllActiveShop,
    findOtherBike,
    countindexNeed,
    filterShopPage,
    filterProducts,
    searchProductByName
};
