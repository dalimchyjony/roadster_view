var express = require('express');
var moment = require('moment');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var validator = require('validator');
var bcrypt = require('bcryptjs');
var _ = require('lodash');
const ShopSchema = require('../models/Shop');
const MemberSchema = require('../models/Members');
const SuperUser = require('../models/Super_users');
const Country = require('../models/Country');
const Division = require('../models/Division');
const Zilla = require('../models/Zilla');
const Thana = require('../models/Thana');
const Product_Bike = require('../models/Bike');
const YouTLink = require('../models/Youtube_link');
const ContactMsg = require('../models/Contact_message');
const CusBook = require('../models/Customers_booking');
const Newsletter = require('../models/Newsletter');
const BazarCustomer = require('../models/Customers');
const Conversations = require('../models/Conversations');
const Messages = require('../models/Messages');
var securePin = require("secure-pin");
var verification_code = securePin.generatePinSync(6);
const BazarBlog = require('../models/Blog_post');
const BlogCommnets = require('../models/Blog_comments');

var salt = bcrypt.genSaltSync(10);

const nodemailer = require("nodemailer");
const { validate } = require('uuid');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'info.autobazarbd24689900',
  }
 });

var cacheAllBlog = {
    data:[],
    time:0
};


var insert_contMsg = (data,callback)=>{

    data['contact_id'] = uuidv4();

    var newContactMsg = new ContactMsg(data);

     newContactMsg.save().then(res =>{
             callback({status:true,data:data});
        })
        .catch(err => console.log(err));

}



var proBookNow =(data,callback)=>{
    data['booking_id'] = uuidv4();
    data['booking_short_id'] = 'AB-'+moment().unix()+'';

    // console.log(data)

    var nowBook = new CusBook(data);

    nowBook.save().then(res =>{
            callback({status:true,data:data});
       })
       .catch(err => console.log(err));
}


var createNewsletter = (data,callback)=>{
  if(data.name == ''){
    data.name = null
  }

  data['newsletter_id'] = uuidv4();

  var newdata = new Newsletter(data);

  newdata.save().then(res =>{
    callback({status:true,data:data});
  }).catch(err =>{
    console.log(err);
  });


}



var findBlogData = (data)=>{
  return new Promise((resolve,reject)=>{
    BazarBlog.paginate({status:true}, data, function(err, result) {
      if(err){
        reject({status:false,err:err});
      }else{
        resolve({status:true,data:result});
      }
    });
  });
}



var findSingleBlog = (blog_id)=>{
  return new Promise((resolve,reject)=>{
    var condition = {blog_id:blog_id}
    if(!validator.isUUID(blog_id)){
      condition = {uniq_url:blog_id}
    }
    BazarBlog.findOne(condition,function(err,result){
      if(err){
        reject({status:false,err:err});
      }else{
        if(result != null){
          BazarBlog.updateOne({blog_id:result.blog_id},{ $inc: { views: 1}},function(incerr,success){
            if(incerr){
              console.log(incerr);
            }else{
              // console.log('Increment 1 views');
            }
          });
          BlogCommnets.find({blog_id:result.blog_id}).sort({created_at: -1}).exec(function(error,allcomments){
            if(err){
               reject({status:false,err:err});
            }else{
              var allcustomer_id = [];
              _.each(allcomments,function(v,k){
                allcustomer_id.push(v.customer_id);
              });
              BazarBlog.paginate({status:true},{page:1,limit:6,sort:{ created_at: -1 }}, function(err2, result2) {
                if(err){
                  reject({status:false,err:err2});
                }else{
                  if(allcustomer_id.length > 0){
                    BazarCustomer.find( {customer_id: { $in : allcustomer_id } } ,function(er,re){
                      if(er){
                        console.log(er);
                      }else{
                        // console.log(134,re);
                        resolve({status:true,data:result,recent:result2,comments:allcomments,customerdata:re});
                      }
                    });
                  }else{
                    resolve({status:true,data:result,recent:result2,comments:allcomments,customerdata:[]});
                  }
                }
              });
            }
          })
        }else{
          reject({status:false,err:err});
        }
      }
    });
  });
}


var createNewCustomer = (data)=>{
  return new Promise((resolve,reject)=>{
     data.password = bcrypt.hashSync(data.password, salt);
     BazarCustomer.findOne({customer_email:data.customer_email},function(err,result){
        if(result == null){
          data['customer_id'] = uuidv4();
          new BazarCustomer(data).save().then((success)=>{
            resolve({status:true,msg:'Successfully Registerd.' ,customer_id:data['customer_id']});
          }).catch((error)=>{
            reject({status:false,error:error});
          });
        }else{
          resolve({status:false,msg:'Email Already Exits.', email:true});
        }
     });

  });
}

var loginCustomer = (data)=>{
  return new Promise((resolve,reject)=>{
     BazarCustomer.findOne({customer_email:data.customer_email},function(err,result){
      if(err){
        console.log(err);
      }else{
        if(result == null){
          resolve({status:false,msg:'Sorry your email not found.', email:false});
        }else{
           bcrypt.compare(data.password, result.password, function(err2, res) {
              if(err2){
                  console.log(err2);
              }else{
                  if(res){
                      resolve({status:true,data:result});
                  }else{
                     resolve({status:false,msg:'Password does not match.', email:true});
                  }
              }
           });
        }
      }
     });

  });
}



var createNewComment = (data)=>{
  return new Promise((resolve,reject)=>{
     data['comments_id'] = uuidv4();
     new BlogCommnets(data).save().then(result =>{
      resolve({status:true,data:data});
     }).catch(err =>{
     resolve({status:false,msg:'Something wrong'});
     })

  });
}
var getConversationId = (data)=>{
  return new Promise((resolve,reject)=>{
    Conversations.findOne({participants:{$eq:[data.customer_id,data.product_owner_id]}},function(err,result){
      if(err){
        console.log(220,err);
        resolve({status:false,msg:'Something wrong'});
      }else{
        if(result == null){
          var newConv = {
            conversation_id:uuidv4(),
            participants:[data.customer_id,data.product_owner_id],
            conv_for:'customers',
            owner_id:data.product_owner_id,
            created_by:data.customer_id
          }
          
          new Conversations(newConv).save().then(result =>{
            resolve({status:true,data:newConv.conversation_id,newConv:newConv});
           }).catch(err =>{
             console.log(234,err);
           resolve({status:false,msg:'Something wrong'});
           })
        }else{
          resolve({status:true,data:result.conversation_id});
        }
      }
    })
  });
}


var getInitialMsg = (data)=>{
  return new Promise((resolve,reject)=>{
    Messages.find({conversation_id:data.conversation_id}).limit(50).exec(function(err,result){
      if(err){
        reject({status:false,msg:'Something Wrong!!'});
      }else{
        resolve({status:true,data:result});
      }
  })
  });
}

var sendMessage = (data)=>{
  return new Promise((resolve,reject)=>{
    var newmsg = {
      msg_id:uuidv4(),
      conversation_id:data.conversation_id,
      sender_id:data.sender_id,
      msg_body:data.msg_body
    }
    new Messages(newmsg).save().then(result =>{
      Conversations.updateOne({conversation_id:data.conversation_id},{last_msg_at:new Date()},function(err){
        if(err){
          console.log(err);
        }
      })
      resolve({status:true,msg:newmsg});
     }).catch(err =>{
     console.log(234,err);
     resolve({status:false,msg:'Something wrong'});
     })
  });
}



var findBike_bike1 = (data,callback)=>{
  Product_Bike.paginate({product_status:1,rsv_status:true,bike_condition:1}, data, function(err, result) {
    if(err){
      callback({status:false,err:err});
    }else{
      callback({status:true,data:result});
    }
  });
}
var findOfferBike_bike1 = (data,callback)=>{
  Product_Bike.paginate({product_status:1,rsv_status:true,bike_offer:true}, data, function(err, result) {
    if(err){
      callback({status:false,err:err});
    }else{
      callback({status:true,data:result});
    }
  });
}


var getbikeWithPaginate = (data,callback)=>{
  var condition = data.condition;
  delete data.condition;
  // console.log(298,data,condition)
  Product_Bike.paginate({product_status:1,rsv_status:true,bike_condition:condition}, data, function(err, result) {
    if(err){
      callback({status:false,err:err});
    }else{
      callback({status:true,data:result});
    }
  });
}
var getofferbikeWithPaginate = (data,callback)=>{
  Product_Bike.paginate({product_status:1,rsv_status:true,bike_offer:true}, data, function(err, result) {
    if(err){
      callback({status:false,err:err});
    }else{
      callback({status:true,data:result});
    }
  });
}







module.exports = {
    insert_contMsg,
    proBookNow,
    createNewsletter,
    findBlogData,
    findSingleBlog,
    createNewCustomer,
    loginCustomer,
    createNewComment,
    getConversationId,
    getInitialMsg,
    sendMessage,
    findBike_bike1,
    getbikeWithPaginate,
    findOfferBike_bike1,
    getofferbikeWithPaginate
};
