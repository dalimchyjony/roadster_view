var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var bcrypt = require('bcryptjs');
var _ = require('lodash');
const CategoryShema = require('../models/Product_category');
const SuperUser = require('../models/Super_users');
const Country = require('../models/Country');
const Division = require('../models/Division');
const Zilla = require('../models/Zilla');
const Thana = require('../models/Thana');
const Banner = require('../models/Banner');
const SliderShema = require('../models/Slider');
const ShopShema = require('../models/Shop');
const Product_Bike = require('../models/Bike');
const BrandShema = require('../models/Product_brand');
const ContactMsg = require('../models/Contact_message');
const BazarBlog = require('../models/Blog_post');
var {getemailTemplate} = require('./foremailtemplate');
var sfs = require('fs');
var securePin = require("secure-pin");
var verification_code = securePin.generatePinSync(6);
var moment = require('moment');

var salt = bcrypt.genSaltSync(10);

const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'AA24689900#',
  }
 });

var cacheSlider = {
    data:[],
    time:0
};

var cacheBrand = {
    data:[],
    time:0
};

var s_user_register =(data,callback)=>{
    var s_user_id = uuidv4();

    var hash = bcrypt.hashSync(data.password, salt);

    var userdata = {
        s_user_id: s_user_id,
        s_user_email: data.email.toLowerCase(),
        s_user_phone : data.phone,
        s_user_name : data.name,
        s_user_role : data.role,
        s_user_img : 'demo.svg',
        s_user_password : hash,
        verification_code:verification_code
    }

    var newSuper_user = new SuperUser(userdata);
    SuperUser.findOne({s_user_email:userdata.s_user_email.toLowerCase()}, function (err, result) {
        if(err){
            console.log(err)
        }else{
            if(result == null){
                newSuper_user.save().then(res =>{
                    callback({msg:'success',data:userdata});
                })
                .catch(err => console.log(err));
            }else{
                callback({msg:'Email already exist.', data:null});
            }

        }
    });
}

var checkVerifiaction = (data,callback)=>{
    SuperUser.findOne({s_user_id:data.s_user_id}, function (err, result) {
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                if(result.verification_code == data.verification_code){
                    if(result.s_user_role == 1){
                        SuperUser.updateOne({s_user_id:data.s_user_id},{email_status:true,s_user_status:true},function(err2,result2){
                            if(err2){
                                console.log(err2);
                            }else{
                                callback({msg:'success', data:null}); 
                            }
                        })
                    }else{
                        SuperUser.updateOne({s_user_id:data.s_user_id},{email_status:true},function(err2,result2){
                            if(err2){
                                console.log(err2);
                            }else{
                                callback({msg:'success', data:null}); 
                            }
                        }) 
                    }
                }else{
                    callback({msg:'Pin does not match.', data:null}); 
                }
            }else{
                callback({msg:'Email does not exist.', data:null}); 
            }
        }
    })
}

var adminLoginCheck = (data,callback)=>{
    SuperUser.findOne({s_user_email:data.email.toLowerCase()}, function (err, result) {
        if(err){
            console.log(err);
        }else{
             if(result !== null){
                 bcrypt.compare(data.password, result.s_user_password, function(err2, res) {
                    if(err2){
                        console.log(err2);
                    }else{
                        if(res == true){
                            if(result.email_status){
                                if(result.s_user_status){
                                    callback({msg:'success',data:result});
                                }else{
                                    callback({msg:'Your account is inactive ! Please contact your admin.',verification:1,data:result});
                                }
                            }else{
                                callback({msg:'Your account is not active. Please check your email & get verification Pin.', verification:2,data:result})
                            }
                        }else{
                            callback({msg:'Password does not match.',verification:3,data:result})
                        }
                    }
                });
             }else{
                 callback({msg:'Email does not exist.',verification:4 ,data:null}); 
             }
        }
    })
}

var findOneAdmin = (data,callback)=>{
    SuperUser.findOne({s_user_id:data.s_user_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                callback({msg:'success', data:result});
            }else{
                callback({msg:'Email does not exist.', data:result});
            }
        }
    })
}

var sendEmailVerification = (data,callback)=>{
    var pinCode = verification_code;
    SuperUser.updateOne({s_user_id:data.s_user_id},{verification_code:pinCode},function(err,result){
        if(err){
            console.log(err);
        }else{
            var toEmail = '';
              if(data.s_user_role == '1'){
                toEmail = ['dalimchyjony@gmail.com','ahn.nayeem@gmail.com'];
              }else{
                toEmail = data.s_user_email;
              }
              var design = '';
                getemailTemplate({type:'superUserVerification',pin:pinCode,msg:'Confirm your email address to get started on AutoBazarBD. Get your verification here.'},function(res){
                    design = res;
                })
              const mailOptions = {
                from: 'info.autobazarbd@gmail.com', // sender address
                to:toEmail, // list of receivers
                subject: 'AutoBazarBD Super user Verification', // Subject line
                html: design// plain text body
              };
              transporter.sendMail(mailOptions, function (err, info) {
                if(err){
                    console.log(185,err)
                  callback({msg:'failed!'})
                }else{
                  callback({msg:'success'});
                }
             });
        }
    })
}

var upEmailVerificaiton = (data,callback)=>{
    SuperUser.findOne({s_user_id:data.s_user_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            if(result !== null){
                if(result.verification_code == data.verification_code){
                    if(result.s_user_role == 1){
                        SuperUser.updateOne({s_user_id:data.s_user_id},{email_status:true,s_user_status:true},function(err2,result2){
                            if(err2){
                                console.log(err2);
                            }else{
                                callback({msg:'success'});
                            }
                        });
                    }else{
                        SuperUser.updateOne({s_user_id:data.s_user_id},{email_status:true},function(err2,result2){
                            if(err2){
                                console.log(err2);
                            }else{
                                callback({msg:'success'});
                            }
                        });
                    }
                }else{
                    callback({msg:'failed!'});
                }
            }else{
                callback({msg:'failed!'});
            }
        }
    })
}

var findAllSuperUser = (callback)=>{
    SuperUser.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var updateSUStatus = (data,callback)=>{
    SuperUser.updateOne({s_user_id:data.s_user_id},{s_user_status:data.s_user_status},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var updateSUEStatus = (data,callback)=>{
    SuperUser.updateOne({s_user_id:data.s_user_id},{email_status:data.eamil_status},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var deleteSuperUser = (data,callback)=>{
    SuperUser.deleteOne({s_user_id:data.s_user_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}


var findOneCountry = (data,callback)=>{
    Country.findOne({country_name:data.country_name},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}


var insertOneCountry = (data,callback)=>{
    var cData = {
        country_id:uuidv4(),
        country_name:data.country_name
    }
    var newCountry = new Country(cData);
    newCountry.save().then(res =>{
        callback({msg:'success',data:cData});
    })
    .catch(err => console.log(err));

}

var findOneDivision = (data,callback)=>{
   Division.findOne({division_name:data.division_name,country_id:data.country_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    }) 
}


var insertOneDivision = (data,callback)=>{
    var dData = {
        country_id:data.country_id,
        division_id:uuidv4(),
        division_name:data.division_name
    }
    var newDivision = new Division(dData);
    newDivision.save().then(res =>{
        callback({msg:'success',data:dData});
    })
    .catch(err => console.log(err));
}


var findOneZilla = (data,callback)=>{
    Zilla.findOne({zilla_name:data.zilla_name,country_id:data.country_id,division_id:data.division_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    }) 
}

var insertOneZilla = (data,callback)=>{
    var zData = {
        country_id:data.country_id,
        division_id:data.division_id,
        zilla_id:uuidv4(),
        zilla_name:data.zilla_name
    }
    var newZilla = new Zilla(zData);
    newZilla.save().then(res =>{
        callback({msg:'success',data:zData});
    })
    .catch(err => console.log(err));
}

var findOneThana = (data,callback)=>{
    Thana.findOne({thana_name:data.thana_name,country_id:data.country_id,division_id:data.division_id,zilla_id:data.zilla_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    }) 
}

var insertOneThana = (data,callback)=>{
    var tData = {
        country_id:data.country_id,
        division_id:data.division_id,
        zilla_id:data.zilla_id,
        thana_id:uuidv4(),
        thana_name:data.thana_name
    }
    var newThana = new Thana(tData);
    newThana.save().then(res =>{
        callback({msg:'success',data:tData});
    })
    .catch(err => console.log(err));
}

var findAllCountry = (callback)=>{
    Country.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}
var findAllDivision = (callback)=>{
    Division.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}
var findAllZilla = (callback)=>{
    Zilla.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}
var findAllThana = (callback)=>{
    Thana.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}


var getDivisionByCountry = (data,callback)=>{
    Division.find({country_id:data.country_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var getZillaByDivision = (data,callback)=>{
    Zilla.find({division_id:data.division_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var getThanaByZilla = (data,callback)=>{
    Thana.find({zilla_id:data.zilla_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}

var getAllCategory = (callback)=>{
    CategoryShema.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            if(result.length == 0){

                var multiData = [
                    {
                        category_id:uuidv4(),
                        category_name:'Bike',
                        category_tbl_name:'Product_bike',
                        category_img:'bike.png'
                    },
                    {
                        category_id:uuidv4(),
                        category_name:'Car',
                        category_tbl_name:'Product_car',
                        category_img:'car.png'
                    },
                    {
                        category_id:uuidv4(),
                        category_name:'Bike Parts',
                        category_tbl_name:'Product_bike_parts',
                        category_img:'bikeparts.png'
                    },
                    {
                        category_id:uuidv4(),
                        category_name:'Car Parts',
                        category_tbl_name:'Product_car_parts',
                        category_img:'carparts.png'
                    },
                ]
                CategoryShema.insertMany(multiData,function(err2,result2){
                    if(err2){
                        console.log(err2);
                    }else{
                        callback({msg:'success',data:result2});
                    }
                });
            }else{
                callback({msg:'success',data:result});
            }
        }
    })
}



var getAllSlider = (callback)=>{
    SliderShema.find().sort({created_at: -1}).exec(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })

   
}

var newSlider = (data,callback)=>{
    var slider_id =  uuidv4()
    data['slider_id'] = slider_id;
    new SliderShema(data).save(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result,slider_id:slider_id})
        }
    });
}
var newBrand = (data,callback)=>{
    var brandid =  uuidv4()
    data['brand_id'] = brandid;
    new BrandShema(data).save(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result,brand_id:brandid});
        }
    });
}


var updateSliderStatus = (data,callback)=>{
    SliderShema.updateOne({slider_id:data.slider_id},{status:data.status},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}

var updateBrandStatus = (data,callback)=>{
    BrandShema.updateOne({brand_id:data.brand_id},{status:data.status},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}
var updateBannerStatus = (data,callback)=>{
    Banner.updateOne({banner_id:data.banner_id},{status:data.status},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}


var findAllShop = (callback)=>{
    ShopShema.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}


var upShopStatus = (data,callback)=>{
    ShopShema.updateOne({shop_id:data.shop_id},{shop_status:data.shop_status},function(err,result){
        if(err){
            console.log(err);
        }else{
            Product_Bike.update({shop_id:data.shop_id},{rsv_status:data.shop_status},function(err){
                console.log(err);
            })
            callback({msg:'success',data:result});
        }
    });
}


var deleteSlider = (data,callback)=>{
    SliderShema.deleteOne({slider_id:data.slider_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}

var deleteBrand = (data,callback)=>{
    BrandShema.deleteOne({brand_id:data.brand_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}
var deleteBanner = (data,callback)=>{
    Banner.deleteOne({banner_id:data.banner_id},function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    });
}

var getAllBrand = (callback)=>{
    BrandShema.find().sort({created_at:-1}).exec(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result})
        }
    })
}

var getContactMsg = (callback)=>{
    ContactMsg.find().sort({created_at:-1}).limit(1000).exec(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({status:true,data:result});
        }
    })
}

var createNewBlog = (data)=>{
    return new Promise((reslove,reject)=>{
        var geturl = data.blog_title;
        geturl = geturl.split(" ").join("-");
        geturl = geturl.split(".").join("-");
        BazarBlog.find({uniq_url:geturl},function(err2,result2){
            if(err2){
                console.log(err2);
            }else{
                if(result2.length == 0){
                    data["uniq_url"] = geturl;
                }else{
                    data["uniq_url"] = geturl+result2.length+1;
                }
                new BazarBlog(data).save(function(err,result){
                    if(err){
                        reject({status:false,error:err})
                    }else{
                        reslove({status:true,data:result});
                    }
                });
            }
        })
        
    })
}
var updateOneBlog = (condition,data)=>{
    return new Promise((reslove,reject)=>{
        BazarBlog.updateOne(condition,data,function(err,result){
            if(err){
                reject({status:false,error:err})
            }else{
                reslove({status:true,data:result});
            }
        });
        
    })
}


var findAllBlogPosts = (callback)=>{
    BazarBlog.find(function(err,result){
        if(err){
            console.log(err);
        }else{
            callback({msg:'success',data:result});
        }
    })
}
var findOneBlogData = (condition)=>{
    return new Promise((resolve,reject)=>{
        BazarBlog.findOne(condition,function(err,result){
            if(err){
                console.log(err);
            }else{
                resolve({msg:'success',data:result});
            }
        })
    });
    
}

var deletePost = (data,callback)=>{
    BazarBlog.deleteOne({blog_id:data.post_id}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            sfs.unlink(data.path, (err) => {
				if (err) {
				  console.error(err);
				  return
				}
			});
            callback({msg:'success'});
        }
    });
}

var publishStatus =(data,callback)=>{
    BazarBlog.updateOne({blog_id:data.post_id},{status:data.type}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            callback({msg: 'success',status: data.type});
        }
    });
}
// var createNewBlog = (data,callback)=>{
//     data['blog_id'] = uuidv4();
//     new BazarBlog(data).save(function(err,result){
//         if(err){
//             callback({status:false,error:err})
//         }else{
//             callback({status:true,data:result});
//         }
//     });
// }

var addNewBanner = (data,callback)=>{
    data['banner_id'] = uuidv4();
    new Banner(data).save(function(err,result){
        if(err){
            callback({status:false,error:err})
        }else{
            callback({status:true,data:data});
        }
    });
}

var findAllBanner = (callback)=>{
    Banner.find(function(err,result){
        if(err){
            callback({status:false,data:[]});
        }else{
            callback({status:true,data:result})
        }
    })
}
var getallactiveBanner = (callback)=>{
    Banner.find({status:true},function(err,result){
        if(err){
            callback({status:false,data:[]});
        }else{
            callback({status:true,data:result})
        }
    })
}

module.exports = {
    s_user_register,
    checkVerifiaction,
    adminLoginCheck,
    findOneAdmin,
    sendEmailVerification,
    upEmailVerificaiton,
    findAllSuperUser,
    updateSUStatus,
    updateSUEStatus,
    deleteSuperUser,
    findOneCountry,
    insertOneCountry,
    findOneDivision,
    insertOneDivision,
    findOneZilla,
    insertOneZilla,
    findOneThana,
    insertOneThana,
    findAllCountry,
    getDivisionByCountry,
    getZillaByDivision,
    findAllDivision,
    findAllZilla,
    findAllThana,
    getThanaByZilla,
    getAllCategory,
    getAllSlider,
    newSlider,
    updateSliderStatus,
    findAllShop,
    upShopStatus,
    deleteSlider,
    getAllBrand,
    newBrand,
    deleteBrand,
    deleteBanner,
    updateBrandStatus,
    updateBannerStatus,
    getContactMsg,
    createNewBlog,
    findAllBlogPosts,
    publishStatus,
    deletePost,
    addNewBanner,
    findAllBanner,
    getallactiveBanner,
    findOneBlogData,
    updateOneBlog
};
