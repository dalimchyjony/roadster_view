var express = require('express');
var router = express.Router();
const nodemailer = require("nodemailer");
var path = require('path');
var siteInfo = require('../config/siteInfo.json');
var _ = require('lodash');
var multer = require('multer');
var inArray = require('in-array');
var fs = require('fs');
var moment = require('moment');
var webp = require('webp-converter');


// creates a configured middleware instance
// destination: handles destination
// filenane: allows you to set the name of the recorded file
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(`./public/images/uploads`))
    },
    filename: function (req, file, callback) {
		// console.log(file)
		var now = Date.now();
		callback(null, file.originalname.replace(path.extname(file.originalname), '@') +now +  path.extname(file.originalname));
    }
});

// var storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//       cb(null, './public/images/uploads')
//     },
//     filename: (req, file, cb) => {
// 		var fileType = file.originalname.split('.');
// 		console.log(fileType[0].split(' ').join('_')+ '@' + Date.now()+'.'+fileType[fileType.length - 1])
//         cb(null, fileType[0].split(' ').join('_')+ '@' + Date.now()+'.'+fileType[fileType.length - 1]);
      
//     }
// });

// utiliza a storage para configurar a instância do multer
const upload = multer({ storage }).array('imageUpload', 1000);
const uploadFile = multer({ storage }).array('uploadImage', 1000);



var {
	findAllCountry,
	getAllBrand
} = require('../utils/super_user');
var {
	findShopEmail,
	findShopMember,
	insertNewShop,
	insertNewMember,
	findOneMemberById,
	sendEmailVerification,
    upEmailVerificaiton,
    memberLoginCheck,
	findMembersByShopId,
	findShopDetails,
	findLast20Bike,
	findBikeByShopId,
	findBikeById,
	findYoulinkByproduct,
	findBookingReq,
	findAllMyConv,
	getAllCustomer
} = require('../utils/member');


function findObj(array,key,val){
	var i;
	for (i = 0; i < array.length; i++) {
		if(array[i][key] == val){
			return array[i];
		}
	}
}

function userRole(role){
	switch(role) {
		case 1:
		  return 'Admin';
		  break;
		case 2:
		  return 'Co-Admin';
		  break;
		case 3:
		  return 'User';
		  break;
	  }
}

/* GET users listing. */
router.get('/', function(req, res) {
    // res.send({ title: 'Express' });
    res.redirect('/ab-member/dashboard');
});

router.get('/booking-request', function(req, res) {
    if(req.session.login_member){
        findShopDetails({shop_id:req.session.shop_id},function(shopD){
			findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
                findBookingReq({shop_id:req.session.shop_id},function(allBooking){
    				var userdata = findObj(allSM.data,'member_id',req.session.member_id);
    				var resData = {
    					title:'Booking Request',
    					page:'booking_request',
    					allSM:allSM.data,
                        shopDetails:shopD.data,
                        allBooking:allBooking.data,
                        moment:moment,
    					_:_,
    					user_data:{
    						user_name:userdata.member_name,
    						user_email:userdata.member_email,
    						user_phone:userdata.member_phone,
    						user_img:userdata.member_img,
    						user_role:userRole(userdata.member_role),
    						user_id:req.session.member_id
    					}
    	
    				}
    				res.render('backend/member/booking_request', resData);
                })
			})
        })
    }else{
        res.redirect('/ab-member/login')
    }
});

router.get('/chat-page', function(req, res) {
    if(req.session.login_member){
        findShopDetails({shop_id:req.session.shop_id},function(shopD){
			findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
				findAllMyConv({owner_id:req.session.shop_id},function(conversation){
					if(conversation.status){
						var userdata = findObj(allSM.data,'member_id',req.session.member_id);
						var resData = {
							title:'Chat',
							page:'chat_page',
							allSM:allSM.data,
							shopDetails:shopD.data,
							moment:moment,
							conversations:conversation.data,
							_:_,
							user_data:{
								user_name:userdata.member_name,
								user_email:userdata.member_email,
								user_phone:userdata.member_phone,
								user_img:userdata.member_img,
								user_role:userRole(userdata.member_role),
								user_id:req.session.member_id
							}	
						}
						res.render('backend/member/chat_page', resData);
					}else{
						// console.log(conversation);
						res.redirect('/ab-member/login');
					}
				});
			})
        })
    }else{
        res.redirect('/ab-member/login')
    }
});

router.get('/dashboard', function(req, res) {
    if(req.session.login_member){
        findShopDetails({shop_id:req.session.shop_id},function(shopD){
			findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
                findLast20Bike({shop_id:req.session.shop_id},function(productBike){
    				var userdata = findObj(allSM.data,'member_id',req.session.member_id);
    				var resData = {
    					title:'Dashboard',
    					page:'dashboard',
    					allSM:allSM.data,
                        shopDetails:shopD.data,
                        productBike:productBike.data,
    					_:_,
    					user_data:{
    						user_name:userdata.member_name,
    						user_email:userdata.member_email,
    						user_phone:userdata.member_phone,
    						user_img:userdata.member_img,
    						user_role:userRole(userdata.member_role),
    						user_id:req.session.member_id
    					}
    	
    				}
    				res.render('backend/member/dashboard', resData);
                });
			})
        })
    }else{
        res.redirect('/ab-member/login')
    }
});
router.get('/add_product', function(req, res) {
	if(req.session.login_member){
		getAllBrand(function(allBrand){
			findShopDetails({shop_id:req.session.shop_id},function(shopD){
				findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
					var userdata = findObj(allSM.data,'member_id',req.session.member_id);
					var resData = {
						title:'Add Product',
						page:'add_product',
						allSM:allSM.data,
						shopDetails:shopD.data,
						allBrand:allBrand.data,
						_:_,
						user_data:{
							user_name:userdata.member_name,
							user_email:userdata.member_email,
							user_phone:userdata.member_phone,
							user_img:userdata.member_img,
							user_role:userRole(userdata.member_role),
							user_id:req.session.member_id
						}
		
					}
					res.render('backend/member/add_product', resData);
				})
			})
		})
    }else{
        res.redirect('/ab-member/login')
    }
});


// upload.array('any_file_chat', 1000),

router.post('/add_product/bikeImage', function(req, res) {
	upload(req,res,function(err){
		    if (req.session.login_member) {
				if (req.files.length < 1) {
					res.json({ 'msg': 'No files were uploaded.' });
				} else {
					// console.log(250,req.files)
					// if(req.files[0].mimetype.indexOf('image/') > -1){
					// 	var newFileName = req.files[0].filename.split('.');
					// 	newFileName[newFileName.length - 1] = 'webp';
					// 	newFileName = newFileName.join('.');
					// 	console.log(247,newFileName)
					// 	webp.cwebp('./public/images/uploads/'+req.files[0].filename,'./public/images/uploads/'+newFileName,"-q 50",function(status,error)
					// 		{
					// 			console.log(250,status,error)
					// 			req.files[0].filename = newFileName;
					// 			res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
					// 		});
					// }else{

					// 	res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
					// }
					res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
				}
			} else {
				res.send({msg:'failed'});
			}
	});
});
router.post('/file_upload', function(req, res) {
	uploadFile(req,res,function(err){
		    if (req.session.login_member) {
				if (req.files.length < 1) {
					res.json({ 'msg': 'No files were uploaded.' });
				} else {
					// if(req.files[0].mimetype.indexOf('image/') > -1){
					// 	var newFileName = req.files[0].filename.split('.');
					// 	newFileName[newFileName.length - 1] = 'webp';
					// 	newFileName = newFileName.join('.');

					// 	webp.cwebp('./public/images/uploads/'+req.files[0].filename,'./public/images/uploads/'+newFileName,"-q 50",function(status,error)
					// 		{
					// 			req.files[0].filename = newFileName;
					// 			res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
					// 		});
					// }else{

					// 	res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
					// }
					res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
				}
			} else {
				res.send({msg:'failed'});
			}
	});
});

router.get('/manage_product', function(req, res) {
	if(req.session.login_member){
        findShopDetails({shop_id:req.session.shop_id},function(shopD){
			findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
				findBikeByShopId({shop_id:req.session.shop_id},function(productBike){
					var userdata = findObj(allSM.data,'member_id',req.session.member_id);
					var resData = {
						title:'Manage Product',
						page:'manage_product',
						allSM:allSM.data,
						shopDetails:shopD.data,
						productBike:productBike.data,
						_:_,
						user_data:{
							user_name:userdata.member_name,
							user_email:userdata.member_email,
							user_phone:userdata.member_phone,
							user_img:userdata.member_img,
							user_role:userRole(userdata.member_role),
							user_id:req.session.member_id
						}
					}
					res.render('backend/member/manage_product', resData);
				});
			});
        });
    }else{
        res.redirect('/ab-member/login')
    }
});

router.get('/edit_product/:bike_id', function(req, res) {
	if(req.session.login_member){
		getAllBrand(function(allBrand){
			findShopDetails({shop_id:req.session.shop_id},function(shopD){
				findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
					findBikeById({shop_id:req.session.shop_id,bike_id:req.params.bike_id.toString()},function(productBike){
						findYoulinkByproduct({product_id:req.params.bike_id.toString()},function(allyouLink){
                            // console.log(allyouLink);
							var userdata = findObj(allSM.data,'member_id',req.session.member_id);
							var resData = {
								title:'Edit Product',
								page:'edit_product',
								allSM:allSM.data,
								shopDetails:shopD.data,
								productBike:productBike.data,
								allBrand:allBrand.data,
								tubeLink:allyouLink.data,
								_:_,
								user_data:{
									user_name:userdata.member_name,
									user_email:userdata.member_email,
									user_phone:userdata.member_phone,
									user_img:userdata.member_img,
									user_role:userRole(userdata.member_role),
									user_id:req.session.member_id
								}
							}
							res.render('backend/member/edit_product', resData);
						});
					});
				});
			});
		})
    }else{
        res.redirect('/ab-member/login')
    }
});


router.get('/manage_user', function(req, res) {
	if(req.session.login_member){
        findShopDetails({shop_id:req.session.shop_id},function(shopD){
			findMembersByShopId({shop_id:req.session.shop_id},function(allSM){
				var userdata = findObj(allSM.data,'member_id',req.session.member_id);
				var resData = {
					title:'Manage Users',
					page:'manage_users',
					allSM:allSM.data,
                    shopDetails:shopD.data,
					_:_,
					user_data:{
						user_name:userdata.member_name,
						user_email:userdata.member_email,
						user_phone:userdata.member_phone,
						user_img:userdata.member_img,
						user_role:userRole(userdata.member_role),
						user_id:req.session.member_id
					}
	
				}
				res.render('backend/member/manage_user', resData);
			})
        })
    }else{
        res.redirect('/ab-member/login')
    }
});


router.get('/login', function(req, res) {
    if(req.session.msg == undefined){
        req.session.msg = null;
    }
    if(!req.session.login_member){
        var renderData = {
            ses_msg : req.session.msg,
            title:siteInfo.site_title+''+' || Member Login',
        }

        req.session.msg = null;
        res.render('backend/member/login', renderData);
    }else{
        res.redirect('/ab-member/dashboard');
    }
    
});

router.get('/logout', function(req, res) {
    req.session.destroy();
    res.redirect('/ab-member/login');    
});

router.post('/login', function(req, response) {
    var data = {
        email:req.body.login_email.toLowerCase(),
        password:req.body.login_password
    }

    memberLoginCheck(data,function(res){
        var alertMsg = res.msg;
        if(res.msg == 'success' && res.data !== null){
            req.session.success = true;
            req.session.login_member = true;
            req.session.msg = null;
            req.session.member_id = res.data.member_id;
            req.session.shop_id = res.data.shop_id;
            response.redirect('/ab-member/dashboard');
        }else if(res.verification == 2){
            req.session.msg = res.msg;
            response.redirect('/ab-member/email-verification/'+res.data.member_id+'');
        }else if(res.verification == 1 || res.verification == 3 || res.verification == 4 ){
            req.session.msg = res.msg;
            response.redirect('/ab-member/login');
        }
    });

});


router.get('/register', function(req, res) {
	if(req.session.msg == undefined){
		req.session.msg = null;
	}
	if(!req.session.login_member){
		findAllCountry(function(result){
			var renderData = {
				ses_msg : req.session.msg,
				title:'Member Register',
				allCountry:result.data,
				_:_,
			}
			req.session.msg = null;
			res.render('backend/member/register',renderData);
		})
	}else{
		res.redirect('/ab-member/dashboard');
	}
    
});

router.post('/register', function(req, res) {
	if(!req.session.login_member){
		var shop_url = req.body.shopname;
		shop_url = shop_url.split(' ');
		shop_url = shop_url.join('-');
		shop_url = shop_url.toLowerCase();
		var data = {
            name:req.body.name,
            email:req.body.email.toLowerCase(),
            shopname:req.body.shopname,
            role:1,
            phone:req.body.number,
            service:req.body.service,
            shoplocation:req.body.shoplocation,
            password:req.body.password,
            country_id:req.body.country,
            division_id:req.body.division,
            zilla_id:req.body.zilla,
            thana_id:req.body.thana,
            shop_url:shop_url,
        } 
        findShopEmail({shop_email:data.email},function(result){
			console.log(483,result)
        	if(result.data == null){
        		findShopMember({member_email:data.email},function(result2){
        			if(result2.data == null){
        				insertNewShop(data,function(result3){
        					if(result3.msg == 'success'){
        						var newdata = {
        							shopdata:result3.data,
        							bodydata:data,
        						}
        						insertNewMember(newdata,function(result4){
        							if(result4.msg == 'success'){
        								res.redirect('/ab-member/email-verification/'+result4.data.member_id+'');
        							}else{
        								req.session.msg = 'Something Wrong!';
        								res.redirect('/ab-member/login');
        							}
        						})
        					}else{
        						// console.log(result3);
        					}
        				});
        			}else{
        				req.session.msg = 'Email already exist.';
        				res.redirect('/ab-member/register');
        			}
        		})
        	}else{
        		req.session.msg = 'Shop already exist.';
        		res.redirect('/ab-member/register');
        	}
        });
	}else{
		res.redirect('/ab-member/dashboard');
	}
    
});


router.get('/email-verification/:id',function(req,res){
	if(req.session.login_member){
    	res.redirect('/ab-member/dashboard');
    }else{
    	findOneMemberById({member_id:req.params.id},function(response){

    		if(response.msg == 'success'){
    			sendEmailVerification({member_id:response.data.member_id,member_email:response.data.member_email}, function(res2){
    				if(res2.msg == 'success'){
    					req.session.msg = 'Please check your email & get verification Pin.';
    					var renderData = {
    						ses_msg : req.session.msg,
							data:response.data,
							title:siteInfo.site_title+''+' || Email Verification',
    					}
    					req.session.msg = null;
    					res.render('backend/member/email_verification', renderData);
    				}else{
    					req.session.msg = 'Something wrong';
    					var renderData = {
    						ses_msg : req.session.msg,
							data:response.data,
							title:siteInfo.site_title+''+' || Email Verification',
    					}
    					req.session.msg = null;
    					res.render('backend/member/email_verification', renderData);

    				}
    			})
    		}else{
    			// req.session.msg = 'Email does not exist.';
    			// res.redirect('/ab-member/login');
    		}
    	})
    }
})

router.post('/email-verification/:id',function(req,res){
    if(req.session.login_member){
        res.redirect('/ab-member/dashboard');
    }else{
        upEmailVerificaiton({member_id:req.body.userid,verification_code:req.body.pin},function(res2){
            if(res2.msg == 'success'){
                req.session.msg = 'Verification successfully.';
                res.redirect('/ab-member/login');
            }else{
                req.session.msg = 'Verification pin does not match!';
                res.redirect('/ab-member/email-verification/'+req.body.userid+'');
            }
        })
    }
})

module.exports = router;