var express = require('express');
var router = express.Router();
const nodemailer = require("nodemailer");
var siteInfo = require('../config/siteInfo.json');
const uuidv4 = require('uuid/v4');
var validator = require('validator');
var multer = require('multer');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var webp = require('webp-converter');
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'info.autobazarbd24689900',
  }
 });

var {s_user_register,
	checkVerifiaction,
	adminLoginCheck,
	findOneAdmin,
	sendEmailVerification,
	upEmailVerificaiton,
	findAllSuperUser,
	findAllCountry,
	findAllDivision,
	findAllZilla,
	findAllThana,
	getAllCategory,
	getAllSlider,
	findAllShop,
	getAllBrand,
	createNewBlog,
	findAllBlogPosts,
	findAllBanner,
	findOneBlogData,
	updateOneBlog
} = require('../utils/super_user');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(`./public/images/uploads`))
    },
    filename: function (req, file, callback) {
        // console.log(file)
        callback(null, file.originalname.replace(path.extname(file.originalname), '@') +Date.now() +  path.extname(file.originalname));
    }
});

const uploadFile = multer({ storage }).array('uploadImage', 1000);
const uploadimg = multer({ storage });

function findObj(array,key,val){
	var i;
	for (i = 0; i < array.length; i++) {
		if(array[i][key] == val){
			return array[i];
		}
	}
}

function userRole(role){
	switch(role) {
		case 1:
		  return 'Admin';
		  break;
		case 2:
		  return 'Co-Admin';
		  break;
		case 3:
		  return 'User';
		  break;
	  }
}

/* GET users listing. */
router.get('/', function(req, res) {
	if(req.session.login){
		res.redirect('/ab-admin/dashboard');
	}else{
		res.redirect('/ab-admin/login');
	}
});
/* GET users listing. */
router.get('/dashboard', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			var resData = {
				title:siteInfo.site_title+''+' || Dashboard',
				page:'dashboard',
				allSU:allSU.data,
				user_data:{
					user_name:userdata.s_user_name,
					user_email:userdata.s_user_email,
					user_phone:userdata.s_user_phone,
					user_img:userdata.s_user_img,
					user_role:userRole(userdata.s_user_role),
					user_id:req.session.user_id
				}

			}
			res.render('backend/super_admin/dashboard',resData );
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

/* GET users listing. */
router.get('/settings', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			var resData = {
				title:siteInfo.site_title+''+' || Settings',
				page:'settings',
				allSU:allSU.data,
				user_data:{
					user_name:userdata.s_user_name,
					user_email:userdata.s_user_email,
					user_phone:userdata.s_user_phone,
					user_img:userdata.s_user_img,
					user_role:userRole(userdata.s_user_role),
					user_id:req.session.user_id
				}
			}
			res.render('backend/super_admin/settings',resData );
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/manage_user', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			
			var resData = {
				title:siteInfo.site_title+''+' || Users',
				page:'manage_user',
				allSU:allSU.data,
				_:_,
				user_data:{
					user_name:userdata.s_user_name,
					user_email:userdata.s_user_email,
					user_phone:userdata.s_user_phone,
					user_img:userdata.s_user_img,
					user_role:userRole(userdata.s_user_role),
					user_id:req.session.user_id
				}

			}
			res.render('backend/super_admin/manage_user', resData);
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/manage-shop', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			findAllShop(function(allShop){
				var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
				
				var resData = {
					title:siteInfo.site_title+''+' || Shop List',
					page:'manage_shop',
					allSU:allSU.data,
					allShop:allShop.data,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
	
				}
				res.render('backend/super_admin/manage_shop', resData);
			})
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});


router.get('/location', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			findAllCountry(function(allCon){
				findAllDivision(function(alldiv){
					findAllZilla(function(allzil){
						findAllThana(function(allTha){
							var resData = {
								title:siteInfo.site_title+''+' || Location',
								page:'location',
								allSU:allSU.data,
								allCountry:allCon.data,
								allDivision:alldiv.data,
								allZilla:allzil.data,
								allThana:allTha.data,
								_:_,
								user_data:{
									user_name:userdata.s_user_name,
									user_email:userdata.s_user_email,
									user_phone:userdata.s_user_phone,
									user_img:userdata.s_user_img,
									user_role:userRole(userdata.s_user_role),
									user_id:req.session.user_id
								}
							}
							res.render('backend/super_admin/location', resData);
						})
					})
				})
			});
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/create_post', function (req, res) {
	if (req.session.login) {
		if(req.session.msg == undefined){
			req.session.msg = null;
		}
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
				var resData = {
					title:siteInfo.site_title+''+'|| Create Post',
					page:'create_post',
					allSU:allSU.data,
					ses_msg:req.session.msg,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
				}
				req.session.msg = null;
				res.render('backend/super_admin/create_post', resData);
		})
	} else {
		res.redirect('/ab-admin/login');
	}
	
});

router.get('/edit-post/:id', async function (req, res) {
	if (req.session.login) {
		if(req.session.msg == undefined){
			req.session.msg = null;
		}
		console.log(273,req.params.id)
		if(!validator.isUUID(req.params.id)){
			condition = {uniq_url:req.params.id}
		  }else{
			condition = {blog_id:req.params.id}
		  }
		var blog_data = await findOneBlogData(condition);
		if(blog_data.data !== null){
			findAllSuperUser(function(allSU){
				var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
					var resData = {
						title:siteInfo.site_title+''+'|| Create Post',
						page:'edit_post',
						allSU:allSU.data,
						ses_msg:req.session.msg,
						_:_,
						blog_data:blog_data.data,
						user_data:{
							user_name:userdata.s_user_name,
							user_email:userdata.s_user_email,
							user_phone:userdata.s_user_phone,
							user_img:userdata.s_user_img,
							user_role:userRole(userdata.s_user_role),
							user_id:req.session.user_id
						}
					}
					req.session.msg = null;
					res.render('backend/super_admin/edit_post', resData);
			})
		}else{
			renderError(res,'404','Blog not found','404','404');
		}
	} else {
		res.redirect('/ab-admin/login');
	}
	
});

router.post('/create_post', uploadimg.single('feature_img'), function (req, res) {
	console.log(236,req.file);
	// console.log(237,req.body);
	if (req.session.login) {
		// var newFileName = req.file.filename.split('.');
		// 	newFileName[newFileName.length - 1] = 'webp';
		// 	newFileName = newFileName.join('.');

		// 	webp.cwebp('./public/images/uploads/'+req.file.filename,'./public/images/uploads/'+newFileName,"-q 50",function(status,error)
		// 	{
		// 		req.file.filename = newFileName;
		var newblog = {
			blog_id:uuidv4(),
			blog_title:req.body.blog_title,
			blog_short_desc:req.body.blog_short_desc,
			blog_long_desc:req.body.blog_long_desc,
			feature_img:req.file.filename,
			author_id:req.session.user_id,
			author_type:'admin',
			status:((req.body.status == 'on')? true:false)
		}
		createNewBlog(newblog).then((result)=>{
			req.session.msg = 'New Blog Save Successfully.';
			res.redirect('/ab-admin/create_post');
		}).catch((error)=>{
			req.session.msg = 'Something Wrong.';
			res.redirect('/ab-admin/create_post');
		});
			// });
	} else {
		res.redirect('/ab-admin/login');
	}
	
});
router.post('/edit-post/:id', uploadimg.single('feature_img'), function (req, res) {
	console.log(236,req.file);
	// console.log(237,req.body);
	if (req.session.login) {
			if(!validator.isUUID(req.params.id)){
				condition = {uniq_url:req.params.id}
			  }else{
				condition = {blog_id:req.params.id}
			  }
			var newblog = {
				blog_title:req.body.blog_title,
				blog_short_desc:req.body.blog_short_desc,
				blog_long_desc:req.body.blog_long_desc,
				status:((req.body.status == 'on')? true:false)
			}
			if(req.file != undefined){
				newblog['feature_img'] = req.file.filename
			}
			updateOneBlog(condition,newblog).then((result)=>{
				req.session.msg = 'Blog Updated Successfully.';
				res.redirect('/ab-admin/edit-post/'+req.params.id);
			}).catch((error)=>{
				req.session.msg = 'Something Wrong.';
				res.redirect('/ab-admin/post_list');
			});
	} else {
		res.redirect('/ab-admin/login');
	}
	
});

router.get('/post_list', function (req, res) {
	if (req.session.login) {
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			findAllBlogPosts(function(allPost) {
				
				var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
					var resData = {
						title:siteInfo.site_title+''+'|| Post List',
						page:'post_list',
						allSU:allSU.data,
						allPost:allPost.data,
						_:_,
						user_data:{
							user_name:userdata.s_user_name,
							user_email:userdata.s_user_email,
							user_phone:userdata.s_user_phone,
							user_img:userdata.s_user_img,
							user_role:userRole(userdata.s_user_role),
							user_id:req.session.user_id
						}
					}
					res.render('backend/super_admin/post_list', resData);
			});
		});
	} else {
		res.redirect('/ab-admin/login');
	}
	
});

router.get('/category', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			getAllCategory(function(allCate){
				var resData = {
					title:siteInfo.site_title+''+'|| Category',
					page:'category',
					allSU:allSU.data,
					allCate:allCate.data,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
	
				}
				res.render('backend/super_admin/category', resData);
			})
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/slider', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			getAllSlider(function(allSlider){
				var resData = {
					title:siteInfo.site_title+''+'|| Slider',
					page:'slider',
					allSU:allSU.data,
					allSlider:allSlider.data,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
	
				}
				res.render('backend/super_admin/slider', resData);
			})
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/brand', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			getAllBrand(function(allBrand){
				var resData = {
					title:siteInfo.site_title+''+'|| Brand',
					page:'brand',
					allSU:allSU.data,
					allBrand:allBrand.data,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
	
				}
				res.render('backend/super_admin/brand', resData);
			})
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});
router.get('/banner', function(req, res) {
	if(req.session.login){
		req.session.msg = null;
		findAllSuperUser(function(allSU){
			var userdata = findObj(allSU.data,'s_user_id',req.session.user_id);
			findAllBanner(function(allBanner){
				var resData = {
					title:siteInfo.site_title+''+'|| Banner',
					page:'banner',
					allSU:allSU.data,
					allBanner:allBanner.data,
					_:_,
					user_data:{
						user_name:userdata.s_user_name,
						user_email:userdata.s_user_email,
						user_phone:userdata.s_user_phone,
						user_img:userdata.s_user_img,
						user_role:userRole(userdata.s_user_role),
						user_id:req.session.user_id
					}
	
				}
				res.render('backend/super_admin/banner', resData);
			});
		})
	}else{
		res.redirect('/ab-admin/login');
	}
});

router.get('/login', function(req, res) {
	if(req.session.msg == undefined){
		req.session.msg = null;
	}

	if(!req.session.login){
		var renderData = {
			ses_msg : req.session.msg,
			title:siteInfo.site_title+''+' || Admin Login',
		}

		req.session.msg = null;
    	res.render('backend/super_admin/login', renderData);
	}else{
		res.redirect('/ab-admin/dashboard');
	}
});

router.post('/login', function(req, response) {
	var data = {
		email:req.body.login_email,
		password:req.body.login_password
	}
	// console.log(data)

    adminLoginCheck(data,function(res){
    	// console.log(res)

	    var alertMsg = res.msg;
        if(res.msg == 'success' && res.data !== null){
        	req.session.success = true;
					req.session.login = true;
					req.session.msg = null;
					req.session.user_id = res.data.s_user_id;
					response.redirect('/ab-admin/dashboard');
        }else if(res.verification == 2){
        	req.session.msg = res.msg;
        	response.redirect('/ab-admin/email-verification/'+res.data.s_user_id+'');
        }else if(res.verification == 1 || res.verification == 3 || res.verification == 4 ){
        	req.session.msg = res.msg;
        	response.redirect('/ab-admin/login');
        }
	});

});

router.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/ab-admin/login');
});

router.get('/register', function(req, res) {
	if(req.session.msg == undefined){
		req.session.msg = null;
	}
	if(!req.session.login){
		var renderData = {
			ses_msg : req.session.msg,
			title:siteInfo.site_title+''+' || Admin Register',
		}
		req.session.msg = null;
    	res.render('backend/super_admin/register',renderData);
	}else{
		res.redirect('/ab-admin/dashboard');
	}
    
});
router.post('/register', function(req, res) {
	if(!req.session.login){
		var data = {
            name:req.body.name,
            email:req.body.email,
            role:req.body.role,
            phone:req.body.number,
            password:req.body.password
        }
    	s_user_register(data,function(res2){
    		if(res2.msg == 'success'){
    			res.redirect('/ab-admin/email-verification/'+res2.data.s_user_id+'');
    		}else{
    			req.session.msg = res2.msg;
    			res.redirect('/ab-admin/register');
    		}
    	});
	}else{
		res.redirect('/ab-admin/dashboard');
	}
    
});

router.get('/email-verification/:id', function(req, res) {
    if(req.session.login){
    	response.redirect('/ab-admin/dashboard');
    }else{
    	findOneAdmin({s_user_id:req.params.id},function(response){

    		if(response.msg == 'success'){
    			sendEmailVerification({s_user_id:response.data.s_user_id,s_user_email:response.data.s_user_email,s_user_role:response.data.s_user_role}, function(res2){
    				if(res2.msg == 'success'){
    					req.session.msg = 'Please check your email & get verification Pin.';
    					var renderData = {
    						ses_msg : req.session.msg,
							data:response.data,
							title:siteInfo.site_title+''+' || Email Verification',
    					}
    					req.session.msg = null;
    					res.render('backend/super_admin/email_verification', renderData);
    				}else{
    					req.session.msg = 'Something wrong';
    					var renderData = {
    						ses_msg : req.session.msg,
							data:response.data,
							title:siteInfo.site_title+''+' || Email Verification',
    					}
    					req.session.msg = null;
    					res.render('backend/super_admin/email_verification', renderData);

    				}
    			})
    		}else{
    			req.session.msg = 'Email does not exist.';
    			res.redirect('/ab-admin/login');
    		}
    	})
    }
});
router.post('/email-verification/:id', function(req, res) {
    if(req.session.login){
    	response.redirect('/ab-admin/dashboard');
    }else{
    	upEmailVerificaiton({s_user_id:req.body.userid,verification_code:req.body.pin},function(res2){
    		if(res2.msg == 'success'){
    			req.session.msg = 'Verification successfully.';
    			res.redirect('/ab-admin/login');
    		}else{
    			req.session.msg = 'Verification pin does not match!';
    			res.redirect('/ab-admin/email-verification/'+req.body.userid+'');
    		}
    	})
    }
});


///////////////////upload image ///////////////////

router.post('/file_upload', function(req, res) {
	uploadFile(req,res,function(err){
	    if (req.session.login) {
			if (req.files.length < 1) {
				res.json({ 'msg': 'No files were uploaded.' });
			} else {
				if(req.files[0].mimetype.indexOf('image/') > -1){
					var newFileName = req.files[0].filename.split('.');
					newFileName[newFileName.length - 1] = 'webp';
					newFileName = newFileName.join('.');

					webp.cwebp('./public/images/uploads/'+req.files[0].filename,'./public/images/uploads/'+newFileName,"-q 50",function(status,error)
						{
							req.files[0].filename = newFileName;
							res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
						});
				}else{

					res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
				}
			}
		} else {
			res.send({msg:'failed'});
		}
	});
});


module.exports = router;
