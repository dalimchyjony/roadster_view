var express = require('express');
var router = express.Router();
var validator = require('validator');
const nodemailer = require("nodemailer");
var siteInfo = require('../config/siteInfo.json');
var _ = require('lodash');
var moment = require('moment');
// var cache = require('memory-cache');
// var cache = require('express-redis-cache')();
// var cache = require('express-redis-cache')({ expire: 7200 });

// configure cache middleware
    // var memCache = new cache.Cache();
    // var cacheMiddleware = (duration) => {
    //     return (req, res, next) => {
    //         let key =  '__express__' + req.originalUrl || req.url
    //         let cacheContent = memCache.get(key);
    //         if(cacheContent){
    //             res.send( cacheContent );
    //             return
    //         }else{
    //             res.sendResponse = res.send
    //             res.send = (body) => {
    //                 memCache.put(key,body,duration*1000);
    //                 res.sendResponse(body)
    //             }
    //             next()
    //         }
    //     }
    // }

// const flatCache = require('flat-cache');
// let cache = flatCache.load('roductsCache');
//  flatCache.clearAll();

// let flatCacheMiddleware = (req,res, next) => {
//     let key =  '__express__' + req.originalUrl || req.url
//     let cacheContent = cache.getKey(key);
//     if( cacheContent){
//         res.send( cacheContent );

//     }else{
//         res.sendResponse = res.send
//         res.send = (body) => {
//             cache.setKey(key,body);
//             cache.save();
//             flatCache.clearAll();
//             res.sendResponse(body)
//         }
//         next()
//     }
// };

var rootQuery = null;
var rootQueryTime = 0;

var customCache = (type)=>{
  console.log(rootQuery,rootQueryTime);
    var now = moment().unix();
    if(type == 'rootQuery'){
      if(rootQueryTime > now){
        return rootQuery;
      }else{
        rootQuery = null;
        return null;
      }
    }
}


var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'info.autobazarbd24689900',
  }
 });

 var {
  getAllSlider,
  getAllBrand,
  findAllShop,
  getallactiveBanner
} = require('../utils/super_user');
 var {
  findBike,
  findSingleBike,
  findAllActiveShop,
  findOtherBike,
  countindexNeed
} = require('../utils/home');
 var {
  findShopUrl,
  findAllBike,
  findShopDetails,
  findYoulinkByproduct
} = require('../utils/member');
 var {
  findBlogData,
  findSingleBlog,
  createNewCustomer,
  loginCustomer,
  findBike_bike1,
  findOfferBike_bike1
} = require('../utils/frontend');

/* GET home page. */
// router.get('/', function(req, res) {
//   var cacheData = customCache('rootQuery');

//   if(cacheData !== null){
//     var resData = {
//         title:siteInfo.site_title,
//         page:'home',
//         allSlider:rootQuery.allSlider,
//         bikes:rootQuery.bikes,
//         allBrand:rootQuery.allBrand,
//         _:_,
//       }

//       res.render('frontend/index', resData);
//   }else{
//       getAllSlider(function(allSlider){
//         findBike({limit:1000},function(bikes){
//           getAllBrand(function(allBrand){
//             var resData = {
//               title:siteInfo.site_title,
//               page:'home',
//               allSlider:allSlider.data,
//               bikes:bikes.data,
//               allBrand:allBrand.data,
//               _:_,
//             }

//             rootQuery = {
//               getAllSlider:allSlider.data,
//               findBike:bikes.data,
//               getAllBrand:allBrand.data
//             };
//             rootQueryTime = moment().unix();

//             res.render('frontend/index', resData);
//           })
//         });
//       });
//   }
// });
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

router.get('/',function(req, res) {
  console.log('without redis')
  getAllSlider(function(allSlider){
    findBike_bike1({page:1,limit:16,sort:{ bike_price: -1 }},function(bikes){
      bikes.data.docs = shuffle(bikes.data.docs);
      getAllBrand(function(allBrand){
        countindexNeed(function(countData){
          var resData = {
            title:siteInfo.site_title,
            page:'home',
            allSlider:allSlider.data,
            bikes:shuffle(bikes.data),
            allBrand:allBrand.data,
            countData:countData.data,
            _:_,
            moment:moment,
          }
          findBlogData({page:1,limit:3,sort:{ created_at: -1 }}).then((result)=>{
            resData['blog'] = result.data.docs;
            res.render('frontend/index', resData);
          }).catch((err)=>{
            res.render(err);
          });
        })
        
      });
    });
  });
});


router.post('/check-has-login',function(req, res) {
    if(req.session.customer_login){
      var user_data = {
        customer_id:req.session.customer_id,
        customer_name:req.session.customer_name,
        customer_email:req.session.customer_email,
        customer_phone:req.session.customer_phone
      }
      res.send({status:true,data:user_data});
    }else{
      res.send({status:false});
    }
});


router.post('/create-new-customer',function(req, res) {
  createNewCustomer(req.body).then((success)=>{
    if(success.status){
      req.session.customer_login = true;
      req.session.customer_id  = req.body.customer_id;
      req.session.customer_name  = req.body.customer_name;
      req.session.customer_email  = req.body.customer_email;
      req.session.customer_phone  = req.body.customer_phone;

      res.send(success);

    }else{
      if(success.email){
         res.send(success);
      }
    }
  }).catch((error)=>{
    console.log(190,error);
  })
});

router.post('/login-customer',function(req, res) {
  loginCustomer(req.body).then((success)=>{
    if(success.status){
      req.session.customer_login = true;
      req.session.customer_id  = success.data.customer_id;
      req.session.customer_name  = success.data.customer_name;
      req.session.customer_email  = success.data.customer_email;
      req.session.customer_phone  = success.data.customer_phone;

      res.send({status:true});

    }else{
      res.send(success)
    }
  }).catch((error)=>{
    console.log(190,error);
  })
});

router.post('/sign-out-customer',function(req, res) {

  req.session.customer_login = false;
  
  res.send({status:true});

});

router.get('/products',(req, res) => {
  findBike_bike1({page:1,limit:16,sort:{ bike_price: -1 }},function(bikes){
    bikes.data.docs = shuffle(bikes.data.docs);
    getAllBrand(function(allBrand){
      var resData = {
        title:'Products',
        page:'products',
        bikes:shuffle(bikes.data),
        allBrand:allBrand.data,
        _:_,
      }
      res.render('frontend/products', resData);

    });
  });

});

router.get('/product/:category/:value', (req, res) => {
  console.log(272,req.params.category)
  if(req.params.category == 'bike'){
    var condition = {
      uniq_url:req.params.value
    }
    if(validator.isUUID(req.params.value)){
      condition = {bike_id:req.params.value}
      
    }
    console.log(281,condition)
    findSingleBike(condition, function(bike) { 
      if(bike.data !== null){
        findShopDetails({shop_id:bike.data.shop_id},function(shopD){
          if(shopD !== null){
            findYoulinkByproduct({product_id:bike.data.bike_id},function (bikeYoulink){
              findOtherBike({shop_id:bike.data.shop_id},function(ob){
                var resData = {
                  title:bike.data.bike_name,
                  page:'single_product',
                  bike:bike.data,
                  shopD:shopD.data,
                  product_cate:req.params.category,
                  product_id:bike.data.bike_id,
                  tubeLink:bikeYoulink.data,
                  otherBike:ob.data,
                  _:_,
                }
                console.log(299,resData)
                res.render('frontend/single_product', resData);
              });
            });
          }else{
            renderError(res,'404','Shop not found','404','404');
          }
        });
      }else{
        renderError(res,'404','Bike not found','404','404');
        
      }
    });
  }
});


router.get('/blog', (req, res) => {
  var resData = {
    title:'Blog',
    page:'blog',
    _:_,
    moment:moment,
    blog:[],
    total_blog:0,
    limit_blog:0,
    page_blog:0,
    pages_blog:0,
  }
  findBlogData({page:1,limit:9,sort:{ created_at: -1 }}).then((result)=>{
    resData['blog'] = result.data.docs;
    resData['total_blog'] = result.data.total;
    resData['limit_blog'] = result.data.limit;
    resData['page_blog'] = result.data.page;
    resData['pages_blog'] = result.data.pages;
    res.render('frontend/blog', resData);
  }).catch((err)=>{
    res.render('frontend/blog', resData);
  })

});

router.get('/blog/page=:pageid', (req, res) => {
  if(req.params.pageid != undefined){
    var resData = {
      title:'Blog',
      page:'blog',
      _:_,
      moment:moment,
      blog:[],
      total_blog:0,
      limit_blog:0,
      page_blog:0,
      pages_blog:0,
    }
    findBlogData({page:req.params.pageid,limit:9,sort:{ created_at: -1 }}).then((result)=>{
      
      resData['blog'] = result.data.docs;
      resData['total_blog'] = result.data.total;
      resData['limit_blog'] = result.data.limit;
      resData['page_blog'] = result.data.page;
      resData['pages_blog'] = result.data.pages;
      // console.log(259,resData)
      res.render('frontend/blog', resData);

    }).catch((err)=>{
      res.render('frontend/blog', resData);
    })
  }else{
    res.redirect('/blog');
  }

});
router.get('/blog/:blog_id', (req, res) => {
  if(req.params.blog_id != undefined){
    findSingleBlog(req.params.blog_id).then((result)=>{
      if(result.data != null){
        var resData = {
          title:result.data.blog_title,
          page:'single_post',
          _:_,
          moment:moment,
          blog:result.data,
          recent_blog:result.recent,
          comments:result.comments,
          customer_data:result.customerdata
        }
        res.render('frontend/single_post', resData);

      }else{
        res.redirect('/blog');
      }
      
    }).catch((err)=>{
      res.redirect('/blog');
    })
    // findBlogData({page:req.params.pageid,limit:9,sort:{ created_at: -1 }}).then((result)=>{
      
    //   resData['blog'] = result.data.docs;
    //   resData['total_blog'] = result.data.total;
    //   resData['limit_blog'] = result.data.limit;
    //   resData['page_blog'] = result.data.page;
    //   resData['pages_blog'] = result.data.pages;
    //   console.log(259,resData)
    //   res.render('frontend/blog', resData);

    // }).catch((err)=>{
    //   res.render('frontend/blog', resData);
    // })
  }else{
    res.redirect('/blog');
  }

});


router.get('/contact', function(req, res) {
    var resData = {
      title:'Contact',
      page:'contact',
      _:_,
    }
    res.render('frontend/contact', resData);
});

router.get('/about-us', function(req, res) {
    var resData = {
      title:'About Us',
      page:'about_us',
      _:_,
    }
    res.render('frontend/about_us', resData);
});

router.get('/user_register', function(req, res) {
    var resData = {
      title:'Register',
      page:'user_register',
      _:_,
    }
    res.render('frontend/user_register', resData);
});

router.get('/showrooms',function(req, res) {
  findAllActiveShop(function(allShop){
    var resData = {
      title:'Showrooms',
      page:'shop',
      allShop:shuffle(allShop.data),
      _:_,
    }
    res.render('frontend/shop', resData);
  })
});

router.get('/showroom/:shop_url',function(req, res) {
  findShopUrl({shop_url:req.params.shop_url},function(shopData){
    if(shopData.msg == 'success'){
      if(shopData.data.shop_status){
        getAllBrand(function(allBrand){
          findAllBike({shop_id:shopData.data.shop_id},function(allBike){
            var resData = {
              title:shopData.data.shop_name,
              page:'shop_single',
              shopD:shopData.data,
              allBike:allBike.data,
              allBrand:allBrand.data,
              _:_,
            }
            res.render('frontend/shop_single', resData);
          });
        });
      }else{
        renderError(res,'Warnning!!!!!','Shop is inactive. Please contact your admin.','404','404');
      }
    }else{
        renderError(res,'404','Shop not found.','404','404');
    }
      
  })
});

router.get('/offer',function(req, res) {
  findOfferBike_bike1({page:1,limit:20,sort:{ bike_price: -1 }},function(bikes){
    bikes.data.docs = shuffle(bikes.data.docs);
    var resData = {
      title:'Offer',
      page:'offer',
      bikes:shuffle(bikes.data),
      _:_,
    }
    res.render('frontend/offer', resData);

  });
});

var renderError=(res,message,error,title,page)=>{
  res.render('error', {
      message: message,
      error: error,
      title:title,
      page:page
  });
}

module.exports = router;
