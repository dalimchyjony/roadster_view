

var allCountrys = [];
var allDivisions = [];
var allZillas = [];
var allThanas = [];
var allShops = [];


function changeLocation(){
	$.each(allShops,function(k,v){
		var address = v.shop_address;
		var thana ='';
		var zilla ='';
		var division ='';
		var country ='';
		$.each(allThanas,function(kt,vt){
			if(vt.thana_id == v.thana_id){
				thana = vt.thana_name
			}
		});
		$.each(allZillas,function(kt,vt){
			if(vt.zilla_id == v.zilla_id){
				zilla = vt.zilla_name
			}
		});
		$.each(allDivisions,function(kt,vt){
			if(vt.division_id == v.division_id){
				division = vt.division_name
			}
		});
		$.each(allCountrys,function(kt,vt){
			if(vt.country_id == v.country_id){
				country = vt.country_name
			}
		});

		$('._locationFull_'+v.shop_id).html(address+','+thana+','+zilla+','+division);
		$('._location_'+v.shop_id).html(address+','+thana);
		$('._shopName_'+v.shop_id).html(v.shop_name);
		$('._shopContact_'+v.shop_id).html(v.shop_phone);

	})
}


function zoomImg (){
	$("#gear").mlens(
	{
		imgSrc: $("#gear").attr("data-big"),	  // path of the hi-res version of the image
		imgSrc2x: $("#gear").attr("data-big2x"),  // path of the hi-res @2x version of the image
													//for retina displays (optional)
		lensShape: "square",                // shape of the lens (circle/square)
		lensSize: ["40%","50%"],            // lens dimensions (in px or in % with respect to image dimensions)
											// can be different for X and Y dimension
		borderSize: 4,                  // size of the lens border (in px)
		borderColor: "#fff",            // color of the lens border (#hex)
		borderRadius: 0,                // border radius (optional, only if the shape is square)
		imgOverlay: $("#gear").attr("data-overlay"), // path of the overlay image (optional)
		overlayAdapt: true,    // true if the overlay image has to adapt to the lens size (boolean)
		zoomLevel: 1,          // zoom level multiplicator (number)
		responsive: true       // true if mlens has to be responsive (boolean)
	});
}
$(function(){

	zoomImg ()


    socket.emit('getShopAllData',function(res){
        if(res.msg == 'success'){
            allCountrys = res.countrys;
            allDivisions = res.divisions;
            allZillas = res.zillas;
            allThanas = res.thanas;
			allShops = res.shops;
			changeLocation();
        }
	});

	socket.emit('getAllDivision',function(res){
		$('#filterDivisions').find('option').remove();
		if(res.msg == 'success'){
			$('#filterDivisions').append('<option class="defalut" value="" selected>Division</option>');
			$.each(res.data,function(k,v){
				$('#filterDivisions').append('<option value="'+v.division_id+'">'+ v.division_name +'</option>');
			})
		}
	});


});
// coll_type
function exp_panel(elm) {
	if ($(elm).attr('aria-expanded') == 'true') {
		// $(elm).siblings('.plus_panel').toggleClass('rotate');
		$(elm).css('color','#FF5555');
		$(elm).parents('.panel-heading').css('background-color','#FFFFFF');
		$(elm).parent('.panel-title').attr('coll_type','plus');
	} else {
		$(elm).css('color','#FFFFFF');
		$(elm).parents('.panel-heading').css('background-color','#FF5555');
		$(elm).parent('.panel-title').attr('coll_type','minus');
	}
}




Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function filterHome(e,elm){
	e.preventDefault();
	var price_range = $('#filterPriceRange').val();
	price_range = price_range.split('-');

	var data = {
		bike_condition :  $('#filterCondition').val(),
		division_id : $('#filterDivisions').val(),
		zilla_id : $('#filterZillas').val(),
		thana_id : $('#filterThanas').val(),
		bike_name:$('#filterBikeName').val().trim(),
		brand_id :  $('#filterBrand').val()
	}

	if(data.bike_condition == ''){
		delete data['bike_condition'];
	}
	if(data.brand_id == ''){
		delete data['brand_id'];
	}
	if(data.division_id == ''){
		delete data['division_id'];
	}
	if(data.zilla_id == ''){
		delete data['zilla_id'];
	}
	if(data.thana_id == ''){
		delete data['thana_id'];
	}

	if(data.bike_name == ''){
		delete data['bike_name'];
	}

	var dataLength = Object.size(data);
	if(dataLength > 0){
		socket.emit('filterHomeBike', data, function(res){
			if(res.msg == 'success'){

				$('.alreadySearched').remove();
				if(res.data.length > 0){
					var datacount = 0;
					$.each(res.data,function(k,v){
						if(v.bike_price > price_range[0] && v.bike_price < price_range[1]){

							datacount = datacount + 1;

							$('#drawSearchDiv').append(returnBikeDesign(v,'alreadySearched'));
						}
					});
					if(datacount > 0){
						$('#searchSection').show();
					}else{
						$('#searchSection').hide();
						Swal.fire({
							type: 'info',
							title: 'Sorry !! No Data Found.',
							showConfirmButton: true
						});

					}
					$('#searchCount').text('SEARCH ('+datacount+')');
					changeLocation();
				}else{
					$('#searchSection').hide();
					Swal.fire({
						type: 'info',
						title: 'Sorry !! No Data Found.',
						showConfirmButton: true
					});
				}
			}
		});
	}else if($('#filterPriceRange').val() == '0-104000000'){
		Swal.fire({
			type: 'warning',
			title: 'Please select any one option',
			showConfirmButton: true
		});
		return false;
	}else{
		socket.emit('filterHomeOnlyRange', {price_range:price_range}, function(res){
			if(res.msg == 'success'){
				$('.alreadySearched').remove();
				if(res.data.length > 0){
					var datacount = 0;
					$.each(res.data,function(k,v){
						if(v.bike_price > price_range[0] && v.bike_price < price_range[1]){
							datacount = datacount + 1;
							$('#drawSearchDiv').append(returnBikeDesign(v,'alreadySearched'));
						}
					});
					if(datacount > 0){
						$('#searchSection').show();
					}else{
						$('#searchSection').hide();
						Swal.fire({
							type: 'info',
							title: 'Sorry !! No Data Found.',
							showConfirmButton: true
						});

					}
					$('#searchCount').text('SEARCH ('+datacount+')');
					changeLocation();
				}else{
					$('#searchSection').hide();
					Swal.fire({
						type: 'info',
						title: 'Sorry !! No Data Found.',
						showConfirmButton: true
					});
				}
			}
		});
	}

}



function returnBikeDesign(v,cssclass){
	var colClass = '';
	if($('.sidebar.with-border').is(':visible')){
		colClass = 'col-lg-4 col-md-4';
	}else{
		colClass = 'col-lg-3 col-md-4';
	}
	var html = '';
		html	+='<div class="car-block '+colClass+' col-sm-6 col-xs-12 '+cssclass+'">';
		html		+='<div class="inner-box">';
		html			+='<div class="image">';
		html				+='<a href="/product/bike/'+(v.uniq_url == null ? v.bike_id:v.uniq_url)+' "><img src="/images/uploads/'+v.feature_img+'" alt="" /></a>';
		if (v.bike_condition == 2) {
			html				+='<div class="status_lable">Used</div>';
		}
		if (v.bike_offer) {
			html				+='<div class="status_lable discount">OFFER</div>';
			html				+='<div class="price">TK. '+v.discount_price+'</div>';
			html				+='<div class="price discount">TK. '+v.bike_price+' </div>';
		} else {
			html				+='<div class="price">TK. '+v.bike_price+' </div>';
		}
		html			+='</div>';
		html			+='<h3><a href="/product/bike/'+(v.uniq_url == null ? v.bike_id:v.uniq_url)+'"> '+v.bike_name+' </a></h3>';
		html			+='<p class="showroom _shopName_'+v.shop_id+'"></p>';
		html			+='<p class="showroom _location_'+v.shop_id+'"></p>';
		html		+='</div>';
		html	+='</div>';
	return html;
}

function filterDivision(elm){
	var value = $(elm).val();
	$('#filterZillas').find('option').remove();
	$('#filterZillas').prepend('<option value="" class="default" selected>Zilla</option>');
	$('#filterZillas').val('');
	if(value !== ''){
		socket.emit('getZillaFilter',{division_id:value},function(res){
			if(res.data == null){
				console.log('not found zilla')
			}else{
				$.each(res.data,function(k,v){
					$('#filterZillas').append('<option value="'+v.zilla_id+'">'+v.zilla_name+'</option>');
				})
			}
		})
	}
}

function filterZilla(elm){
	var value = $(elm).val();
	$('#filterThanas').find('option').remove();
	$('#filterThanas').prepend('<option value="" class="default" selected>Thana</option>');
	$('#filterThanas').val('');
	if(value !== ''){
		socket.emit('getThanaFilter',{zilla_id:value},function(res){
			if(res.data == null){
				Swal.fire({
					type: 'info',
					title: 'Sorry !! No Zilla Found.',
					showConfirmButton: true
				});
			}else{
				$.each(res.data,function(k,v){
					$('#filterThanas').append('<option value="'+v.thana_id+'">'+v.thana_name+'</option>');
				})
			}
		})
	}
}

function previewPro(elm) {
	let srcv = $(elm).attr('src');
	$('#gear').attr('src',srcv).attr('data-big',srcv);
	zoomImg ()
}



$(function(){
	lazyCall();
})

$(function(){
	$("iframe[data-src]").Lazy({
		scrollDirection: 'both',
		effect: 'fadeIn',
		visibleOnly: true,
		effectTime: 200,
		delay: 2500, // just for demo
		onError: function(element) {
			console.log('error loading ' + element.data('src'));
		}
	});
})

function lazyCall(){
	$('.lazy').lazy({
		beforeLoad: function(element) {
			// called before an elements gets handled
			$(element).removeAttr('no_bg');
        },
		effect: 'fadeIn',
		visibleOnly: true,
		effectTime: 200,
		delay: 2000, // just for demo
		afterLoad: function(element) {
			// called after an element was successfully handled
			$(element).attr('no_bg','1');
        },
		onError: function(element) {
			console.log('error loading ' + element.data('src'));
		}
	});
}

function booknow(e){
	e.preventDefault();
	var data={
		customer_name:$('#bookName').val(),
		customer_phone:$('#bookPhone').val(),
		customer_email:$('#bookEmail').val(),
		product_id:$('#proidVal').val(),
		shop_id:$('#shopIdVal').val(),
		product_type:$('#productCate').val()
	}
	requestBookingProduct(data);

}

function requestBookingProduct(data){
	socket.emit('bookNow',data,function(res){
		if(res.status){
			Swal.fire({
				type: 'success',
				title: 'Your booking id: "'+res.data.booking_short_id+'". Please waiting for call. Thank You',
				showConfirmButton: true,
				confirmButtonText: 'Ok'
			  }).then((result) => {
				if (result.value) {
					location.reload();
				}
			});
		}
	});
}


function submitNewsletter(e,elm){
	e.preventDefault();
	var data = {
		name:$(elm).find('input[name="name"]').val(),
		email:$(elm).find('input[name="email"]').val()
	}
	socket.emit('createNewsletter',data,function(res){
		if(res.status){
			Swal.fire({
				title: 'Thank you for Subscribing.',
				type: 'success',
				showConfirmButton: true,
				confirmButtonText: 'Ok'
			  }).then((result) => {
				if (result.value) {
					location.reload();
				}
			  })
		}
	})
}

function registerCustomer(evt){
	evt.preventDefault();
	evt.stopImmediatePropagation();
	var data = {
		customer_name:$('#registerCustomer [name="customer_name"]').val().trim(),
		customer_email:$('#registerCustomer [name="customer_email"]').val().trim(),
		customer_phone:$('#registerCustomer [name="customer_phone"]').val().trim(),
		password:$('#registerCustomer [name="password"]').val()
	}

	if(data.customer_name == ''){
		Swal.fire({
			type: 'warning',
			title: 'Your name is reqired.',
			showConfirmButton: true
		});
		return false;
	}
	if(data.customer_email == ''){
		Swal.fire({
			type: 'warning',
			title: 'Your email is reqired.',
			showConfirmButton: true
		});
		return false;
	}
	if(data.customer_phone == ''){
		Swal.fire({
			type: 'warning',
			title: 'Your phone is reqired.',
			showConfirmButton: true
		});
		return false;
	}
	if(data.password == ''){
		Swal.fire({
			type: 'warning',
			title: 'Password is reqired.',
			showConfirmButton: true
		});
		return false;
	}

	$.ajax({
        url: "/create-new-customer",
        type: "post",
        data: data,
        success:function(res){
        	if(res.status){
				Swal.fire({
					type: 'success',
					title: 'Successfully Registerd.',
					showConfirmButton: false,
					timer: 1500
				});
				$('.backwrap').hide();
				checkHaslogin();
        	}else{
				Swal.fire({
					type: 'warning',
					title: res.msg,
					showConfirmButton: true
				});
        	}

        },
        error:function(err){
        	console.log(err)
        }
    });

	console.log(data);
}

function loginCustomer(evt){
	evt.preventDefault();
	evt.stopImmediatePropagation();
	var data = {
		customer_email:$('#customerLogin [name="customer_email"]').val().trim(),
		password:$('#customerLogin [name="password"]').val()
	}

	if(data.customer_email == ''){
		Swal.fire({
			type: 'warning',
			title: 'Your email is reqired.',
			showConfirmButton: true
		});
		return false;
	}

	if(data.password == ''){
		Swal.fire({
			type: 'warning',
			title: 'Your email is reqired.',
			showConfirmButton: true
		});
		return false;
	}

	$.ajax({
        url: "/login-customer",
        type: "post",
        data: data,
        success:function(res){
        	if(res.status){
				$('.backwrap').hide();
				// alert('Login succussfully.');
				Swal.fire({
					type: 'success',
					title: 'Login succussfully.',
					showConfirmButton: false,
					timer: 1500
				});
        		checkHaslogin();
        	}else{
				Swal.fire({
					type: 'warning',
					title: res.msg,
					showConfirmButton: true
				});

        	}


        },
        error:function(err){
        	console.log(err)
        }
    });
}

var customer_login = false;
var customer_id = null;
var customer_email = null;
var customer_name = null;
var customer_phone = null;

var conversation_id = null;

function checkHaslogin(){
	var product_owner_id = $('#product_owner_id').val();
	$.ajax({
        url: "/check-has-login",
        type: "post",
        success:function(res){
        	if(res.status){
        		customer_id = res.data.customer_id;
        		customer_email = res.data.customer_email;
        		customer_name = res.data.customer_name;
        		customer_phone = res.data.customer_phone;
        		customer_login = true;

        		$('#accountHasLogin').html(res.data.customer_name);
        		$('#customersingoutbtn').show();
        		$('#customerLoginbtn').hide();

        		$('#post_comment_box').show();
        		$('#login_msg_for_comment').hide();
				$('#msgContainer').show();
				$('#loginMsgContainer').hide();
				console.log(474,product_owner_id)
				if(product_owner_id != undefined){
					socket.emit('getConversationId',{customer_id,product_owner_id},function(res){
						console.log(476,res)
						if(res.status){
							conversation_id = res.data;
							getInitialMsg(conversation_id)
						}
					})
				}
        	}else{
        		$('#accountHasLogin').html('My Account');
        		$('#customersingoutbtn').hide();
        		$('#customerLoginbtn').show();

        		$('#post_comment_box').hide();
				$('#login_msg_for_comment').show();
				$('#msgContainer').hide();
				$('#loginMsgContainer').show();
				$('#msgContainer').html('');
				conversation_id = null;
        	}
        },
        error:function(err){
        	console.log(err)
        }
    });
}

function getInitialMsg(convid){
	socket.emit('getInitialMsg',{conversation_id},function(msg){
		if(msg.status){
			$('#msgContainer').html('');
			$.each(msg.data,function(k,v){

				draw_msg(v);
			});
			var scrollHeight = $("#msgContainer").get(0).scrollHeight;
			$("#msgContainer").animate({ scrollTop: scrollHeight });
		}else{
			console.log(msg);
		}
	});
}

function enterTosend(e){
	if(e.keyCode == 13){
		msg_submit();
	}
}

function draw_msg(msg){
	var whoisSender = '';
	var profileimage = '';
	if(msg.sender_id == customer_id){
		whoisSender = 'from_customer';
		profileimage = '/images/demo_user.png'
	}else{
		whoisSender = 'from_admin';
		var adminImg = $("#productShopImage").val();
		if(adminImg == "null" || adminImg == null ){

			profileimage = '/images/uploads/bike_loader.gif';
		}else{
			profileimage = '/images/uploads/'+adminImg+'';
		}
	}
	var msg_date = moment(msg.created_at).format('hh:mm A DD-MMM-YYYY');
	if(msg_date == undefined){
		msg_date = moment().format('hh:mm A DD-MMM-YYYY');
	}
	var html = '<div class="single_msg" msg_type="'+whoisSender+'">'
					+'<div class="usr_img">'
						+'<img src="'+profileimage+'" alt="">'
					+'</div>'
					+'<div class="msg_body">'
						+'<p class="main_msg_txt">'+msg.msg_body+'</p>'
						+'<p class="msg_date_time">'+msg_date+'</p>'
					+'</div>'
				+'</div>';
		$('#msgContainer').append(html)
}

$('body').on('keydown','#msgContent',function(e){
	if(e.keyCode == 13){
		e.preventDefault();
		e.stopImmediatePropagation();
	}
})

function stripScripts(s) {
	var div = document.createElement('div');
	div.innerHTML = s;
	var scripts = div.getElementsByTagName('script');
	var i = scripts.length;
	while (i--) {
		scripts[i].parentNode.removeChild(scripts[i]);
	}
	return div.innerHTML;
}

function msg_submit(){
	var msg = $('#msgContent').text();
			msg = stripScripts(msg);
	$('#msgContent').html("");
	var data = {
		msg_body:msg,
		sender_id:customer_id,
		conversation_id:conversation_id,
		receiver_id:$('#product_owner_id').val()

	}
	if(data.msg_body !== ''){
		if(conversation_id != null){

			socket.emit('sendMessage',data,function(res){
				console.log(res);
				if(res.status){
					draw_msg(res.msg);
					var scrollHeight = $("#msgContainer").get(0).scrollHeight;
					$("#msgContainer").animate({ scrollTop: scrollHeight });
				}
			})
		}else{
			$('#customerLoginbtn').trigger('click');
		}
	}
}

function customerLogout(){
	$.ajax({
        url: "/sign-out-customer",
        type: "post",
        success:function(res){
        	checkHaslogin();
        },
        error:function(err){
        	console.log(err)
        }
    });
}


checkHaslogin();


function showBooking(){
	if(customer_login){
		var data={
			customer_id:customer_id,
			customer_name:customer_name,
			customer_phone:customer_phone,
			customer_email:customer_email,
			product_id:$('#porduct_id_per_page').val(),
			shop_id:$('#product_owner_id').val(),
			product_type:$('#product_category_per_page').val()
		}
		requestBookingProduct(data);
	}else{

		$('#backwrapPopup').css('display','flex');
	}
}

function postNewComment(evt){
	evt.preventDefault();
	evt.stopImmediatePropagation();

	var data = {
		customer_id:customer_id,
		blog_id:$('#blog_id').val(),
		comments:$('#post_comment_box [name="comments"').val().trim()
	}

	if(data.comments == ''){
		Swal.fire({
			type: 'warning',
			title: 'Comment is reqired.',
			showConfirmButton: true
		});
		return false;
	}
	$('#post_comment_box [name="comments"').val('');
	socket.emit('createNewComment',data,(res)=>{
		var html = '<div class="comment-box">'
					    +'<div class="comment">'
					       +' <div class="author-thumb"><img src="/images/demo_user.png" alt=""></div>'
					        +'<div class="comment-inner">'
					            +'<div class="comment-info clearfix">'+customer_name+' – '+moment(data.created_at).format('MMM DD , YYYY')+':</div>'
					            +'<div class="text">'+data.comments+'</div>'
					        +'</div>'
					    +'</div>'
					+'</div>';

		$('#comentsCount').after(html);

		var totalcomment = (parseInt($('#comentsCount').attr('data-le')) + 1);

		$('#comentsCount').attr('data-le',totalcomment);
		$('#comentsCount').find('h2').html('Comments ('+totalcomment+')');


	});
}


socket.on('new_message',function(res){
   if(res.receiver_id == customer_id && res.msg.conversation_id == conversation_id){
       draw_msg(res.msg);
       var scrollHeight = $("#msgContainer").get(0).scrollHeight;
       $("#msgContainer").animate({ scrollTop: scrollHeight });
   }
});


function findAbike() {
	var price_range = $('#selectPriceRange').val();
		price_range = price_range.split('-');
	var data = {
		bike_condition: $('#selectCondition').val(),
		brand_id : $('#selectBrand').val(),
		rsv_status: true,
		shop_id:$('#shop_id_for_filter').val()
	}
	socket.emit('filterProducts',data, function (res) {
		if (res.status) {
			if (res.data.length > 0) {
				var countData = 0;
				$('#filtrdProducts').show();
				$('.filtrdProducts').remove();

				$.each(res.data,function(k,v){
					if(v.bike_price > price_range[0] && v.bike_price < price_range[1]){
						console.log(219, k);
						countData = countData + 1;
						var html =  returnBikeDesign(v,'filtrdProducts');

						$('#filtrdProducts').append(html);
					}
				});
				if(countData == 0){
					$('#filtrdProducts').hide();
					Swal.fire({
						type: 'warning',
						title: 'Sorry!! No Bikes Found.',
						showConfirmButton: true
					});
				}else{

					$('#searchResults').text('Search Results ('+countData+')');
					lazyCall();
				}
			}else{
				$('#filtrdProducts').hide();
				Swal.fire({
					type: 'warning',
					title: 'Sorry!! No Bikes Found.',
					showConfirmButton: true
				});
			}
		}
	});
}

function searchProductByBtn(ele) {
	if ($('#productsSearchField').val().trim() != '') {

		var data = {
			value: $('#productsSearchField').val().trim(),
			rsv_status: true,
			shop_id:$('#shop_id_for_filter').val()
		}
		socket.emit('searchProductByName',data, function (res) {
			if (res.status) {
				if (res.data.length > 0) {
					var countData = 0;
					$('#filtrdProducts').show();
					$('.filtrdProducts').remove();

					$.each(res.data,function(k,v){
							countData = countData + 1;
							var html =  returnBikeDesign(v,'filtrdProducts');

							$('#filtrdProducts').append(html);
					});
					$('#searchResults').text('Search Results ('+countData+')');
					lazyCall();
				}else{
					$('#filtrdProducts').hide();
					Swal.fire({
						type: 'warning',
						title: 'Sorry!! No Bikes Found.',
						showConfirmButton: true
					});
				}
			}
		});
	}
}

function searchProductByName(e) {
	if (e.keyCode == 13) {
		$('#productsSearchBtn').trigger('click');
	}
}

function showMoreProduct(type,condition,page){
	if(type == 'bike'){
		var condition = Number(condition);
		socket.emit('getbikeWithPaginate',{condition:condition, page:Number(page),limit:16,sort:{ bike_price: -1 }},function(res){
			if(res.status){
				if(condition == 2){
					if(Number(page) == 1){
						$('#usedBikeRow').html('');
					}
				}
				$.each(res.data.docs,function(k,v){
					var html = returnBikeDesign(v);
					if(condition == 1){
						$('#newBikeRow').append(html);
					}else{
						$('#usedBikeRow').append(html);
					}

				});
				$('#show_more_'+type+'_'+condition+'_'+page).remove();
				lazyCall();
				changeLocation();
				if(res.data.pages > res.data.page){
					var btn = '<button class="theme-btn btn-style-one showMoreBtn" onclick="showMoreProduct(\'bike\','+condition+','+((res.data.page) + 1)+')" id="show_more_bike_'+condition+'_'+((res.data.page) + 1)+'"> <span class="icon fa fa-plus"> </span> Show More</button>'
					if(condition == 1){
						$('#newBikeRow').append(btn);
					}else{
						$('#usedBikeRow').append(btn);
					}

				}

			}
		});
	}else if(type == 'offerBike'){
		socket.emit('getofferbikeWithPaginate',{ page:Number(page),limit:20,sort:{ bike_price: -1 }},function(){
			if(res.status){
				$.each(res.data.docs,function(k,v){
					var html = returnBikeDesign(v);
					$('#offerBike_row').append(html);
				});
				$('#show_more_'+type+'_'+condition+'_'+page).remove();
				lazyCall();
				changeLocation();
				if(res.data.pages > res.data.page){
					var btn = '<button class="theme-btn btn-style-one showMoreBtn" onclick="showMoreProduct(\'bike\','+condition+','+((res.data.page) + 1)+')" id="show_more_bike_'+condition+'_'+((res.data.page) + 1)+'"> <span class="icon fa fa-plus"> </span> Show More</button>'
					$('#offerBike').append(btn)

				}
			}
		})
	}
}
