// Custom Javascripts

function removeA(arr) {
	var what, a = arguments, L = a.length, ax;
	while (L > 1 && arr.length) {
		what = a[--L];
		while ((ax = arr.indexOf(what)) !== -1) {
			arr.splice(ax, 1);
		}
	}
	return arr;
}

function productTab(elm, type) {
    $(elm).parent('li').siblings('li').find('.nav-link.active').removeClass('active');
    $(elm).addClass('active');
    if (type == 'bike') {
        $('#bikeTab').siblings('.tab-pane.active').removeClass('active');
        $('#bikeTab').addClass('active');
    } else if (type == 'bikeParts') {
        $('#bikePartsTab').siblings('.tab-pane.active').removeClass('active');
        $('#bikePartsTab').addClass('active');
    } else if (type == 'car') {
        $('#carTab').siblings('.tab-pane.active').removeClass('active');
        $('#carTab').addClass('active');
    } else if (type == 'carParts') {
        $('#carPartsTab').siblings('.tab-pane.active').removeClass('active');
        $('#carPartsTab').addClass('active');
    }
}

$(document).ready(function(){
    $("#bikeForm").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }
            var form = $(this);
            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }
            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";
            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }
            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);
            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";
            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);
            // Submit form input
            // form.submit();
            submitBikeForm(form);
        }
    }).validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    $("#car_form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }
            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }
            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);
            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
});

var bikeFeatureImg = 'img_loader.gif';
var bikeImgGallery = [];

var allBrandList = [];

$(function(){
    socket.emit('getAllBrand',function(res){
        allBrandList = res.data;
    });
})

function submitBikeForm(elm){
    productFromData = true;
    productFromDataMsg = [];
    $('#bikeForm [name]').removeClass('required');
    var  brand_id = bikeFromData('brand_id');
    var brand_name = '';

    $.each(allBrandList,function(k,v){
        if(v.brand_id == brand_id){
            brand_name = v.brand_name;
        }
    })

    var data = {
        shop_id:$('#shopID').val(),
        country_id:$('#country_id').val(),
        division_id:$('#division_id').val(),
        zilla_id:$('#zilla_id').val(),
        thana_id:$('#thana_id').val(),
        shop_address:$('#shop_address').val(),
        bike_name:bikeFromData('bike_name',true),
        bike_price:bikeFromData('bike_price',true),
        discount_price:bikeFromData('discount_price'),
        bike_condition:bikeFromData('bike_condition'),
        bike_model:bikeFromData('bike_model',true),
        feature_img:bikeFeatureImg,
        img_gallery:bikeImgGallery,
        currency:bikeFromData('currency',true),
        quantity:bikeFromData('quantity',true),
        short_desc:bikeFromData('short_desc'),
        long_desc:bikeFromData('long_desc'),
        brand_id: bikeFromData('brand_id'),
        bike_overview:{
            brand_name: brand_name,
            brand_id: bikeFromData('brand_id'),
            type: bikeFromData('type'),
            fuel_supply_system: bikeFromData('fuel_supply_system'),
            starting_method: bikeFromData('starting_method'),
            cooling_system: bikeFromData('cooling_system'),
            engine_oil_recommendtion: bikeFromData('engine_oil_recommendtion'),
            top_speed: bikeFromData('top_speed'),
            standard_warranty_years: bikeFromData('standard_warranty_years'),
            standard_warranty_km: bikeFromData('standard_warranty_km'),
            is_feature: bikeFromData('is_feature'),
            model_year: bikeFromData('model_year'),
            registration_y: bikeFromData('registration_y'),
            owner_type: bikeFromData('owner_type'),
            condition: bikeFromData('condition'),
            reg_number: bikeFromData('reg_number')
        },
        engine:{
            engine_desc: bikeFromData('engine_desc'),
            maximum_power: bikeFromData('maximum_power'),
            minimum_torque: bikeFromData('minimum_torque'),
            bore: bikeFromData('bore'),
            stroke: bikeFromData('stroke'),
            compression_ratio: bikeFromData('compression_ratio'),
            displacement_cc: bikeFromData('displacement_cc'),
            ignition: bikeFromData('ignition'),
            no_of_cylinders: bikeFromData('no_of_cylinders'),
            fuel_system: bikeFromData('fuel_system'),
            fuel_type: bikeFromData('fuel_type'),
            valves_percylinder: bikeFromData('valves_percylinder'),
            mileage_kmpl: bikeFromData('mileage_kmpl'),

        },
        transmission:{
            transmission_type: bikeFromData('transmission_type'),
            no_of_gears: bikeFromData('no_of_gears'),
            clutch_type: bikeFromData('clutch_type'),

        },
        chassis_n_suspension:{
            chassis_type: bikeFromData('chassis_type'),
            suspension_front:bikeFromData('suspension_front'),
            suspension_rear: bikeFromData('suspension_rear'),

        },
        brakes:{
            front_brake_type: bikeFromData('front_brake_type'),
            rear_brake_type: bikeFromData('rear_brake_type'),
            front_brake_diameter: bikeFromData('front_brake_diameter'),
            rear_brake_diameter: bikeFromData('rear_brake_diameter'),
            anti_lock_braking_system_ABS: bikeFromData('anti_lock_braking_system_ABS'),

        },
        wheels_n_tires:{
            tire_size_front:bikeFromData('tire_size_front'),
            tire_size_rear: bikeFromData('tire_size_rear'),
            tire_type: bikeFromData('tire_type'),
            wheels_size_front: bikeFromData('wheels_size_front'),
            wheels_size_rear: bikeFromData('wheels_size_rear'),
            wheels_type: bikeFromData('wheels_type'),

        },
        dimensions:{
            overall_length: bikeFromData('overall_length'),
            overall_width: bikeFromData('overall_width'),
            overall_height: bikeFromData('overall_height'),
            ground_clearance: bikeFromData('ground_clearance'),
            weight: bikeFromData('weight'),
            fuel_tank_capacity: bikeFromData('fuel_tank_capacity'),
            wheelbase_mm: bikeFromData('wheelbase_mm'),
            seat_height_mm: bikeFromData('seat_height_mm'),

        },
        electricals:{
            battery_type: bikeFromData('battery_type'),
            battery_voltage: bikeFromData('battery_voltage'),
            head_light: bikeFromData('head_light'),
            tail_light: bikeFromData('tail_light'),
            indicators: bikeFromData('indicators'),
            pilot_lamps: bikeFromData('pilot_lamps'),

        },
        features:{
            speedometer: bikeFromData('speedometer'),
            odometer: bikeFromData('odometer'),
            rpm_meter: bikeFromData('rpm_meter'),
            handle_type: bikeFromData('handle_type'),
            seat_type: bikeFromData('seat_type'),
            passenger_grab_rail: bikeFromData('passenger_grab_rail'),
            engine_kill_switch: bikeFromData('engine_kill_switch'),
            digital_fuel_indicators: bikeFromData('digital_fuel_indicators'),
            low_fuel_warning_lamp: bikeFromData('low_fuel_warning_lamp'),
            pass_switch: bikeFromData('pass_switch'),
            tachometer: bikeFromData('tachometer'),

        },
        product_status:bikeFromData('status',true),
        created_by:$('#userID').val(),
        youtube_link1:bikeFromData('youtube_link1'),
        youtube_link2:bikeFromData('youtube_link2'),
        youtube_link3:bikeFromData('youtube_link3'),

    }
    console.log(320,bikeFeatureImg)

    if(pageTitle == 'edit_product'){
        var tubeLInk = [
            {
                link_id:$("#bikeForm [name='youtube_link1']").attr('data-id'),
                link:bikeFromData('youtube_link1')
            },
            {
                link_id:$("#bikeForm [name='youtube_link2']").attr('data-id'),
                link:bikeFromData('youtube_link2')
            },
            {
                link_id:$("#bikeForm [name='youtube_link3']").attr('data-id'),
                link:bikeFromData('youtube_link3')
            },
        ];

        delete data['youtube_link1'];
        delete data['youtube_link2'];
        delete data['youtube_link3'];

        if(productFromData){

            socket.emit('updateBike',{data:data,bike_id:bikeID},function(res){
                if(res.msg == 'success'){
                    socket.emit('updateLinkGroup',tubeLInk);
                    location.reload();
                }else{
                    alert('error!!!!!!!!!!!!!')
                }
            })
        }else{
            alert('Required red field.');
        }

    }else{
        if(productFromData){

            socket.emit('addBike',data,function(res){
                if(res.msg == 'success'){
                    location.reload();
                }else{
                    alert('error!!!!!!!!!!!!!')
                }
            })
        }else{
            alert(''+productFromDataMsg+' Required red field. ');
        }
    }
}

var productFromData = true;
var productFromDataMsg = [];

var bikeFromData = (name,required=false)=>{
    var event = document.createEvent('event');
    var value = $("#bikeForm [name="+name+"]").val();
    console.log(required)
    if(required){
        if(value !== ""){
            return value
        }else{
            productFromData = false;
            productFromDataMsg.push(name+' is required.');
            $("#bikeForm [name="+name+"]").addClass('required');
            return value
        }
    }else{

        return value
    }
}

var formDataTemp = [];
var count_files = 0;

function uploadAttachFileServer (files){
        var request = [];
        formDataTemp.length = 0;
    for (var i = 0; i < files.length; i++){
        if(count_files > 19) {
            alert("You can upload max 20 files at a time");
            return false;
        }
        var formData = new FormData();
        var entries = formData.entries();
        for(var pair of entries )
        {
           formData.delete( pair[0] );
        }
        var has_already = false;
        formDataTemp.forEach(function(vv) {
            if (vv.name == files[i].name) {
                has_already = true;
            }
        });
        if (has_already === true) continue;
        var slid = Number(moment().unix()) + i + 1;

        // var fileName = files[i].name.split('.');
        // files[i].name = fileName[0]+'@'+slid+'.'+fileName[fileName.length -1];
        // console.log(files[i].name )

        formDataTemp.push(files[i]);

        formData.append('imageUpload', files[i]);
        count_files++;
        
        formData.append('sl', slid);

        if (files[i].type.startsWith('image/')) {
            var imgsrc = window.URL.createObjectURL(files[i]);
        }

        var imgalt = window.URL.createObjectURL(files[i]);
        var stopthis = () => {
            this.abort();
        };

        request[slid] = $.ajax({
            xhr: function() {
                // $('.fileno_' + slid).find('.chat_file_progress').show();
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("loadstart", function(et) {
                    console.log(et);
                    this.progressId = slid;
                    var html = '<div class="UploadableImage" id="image_'+this.progressId+'">'
                                    +'<span class="close_preview" onclick="unlinkthisFile(this)" data-value="">+</span>'
                                    +'<div class="img_file">'
                                    +' <img src="' + imgsrc + '" alt="' + imgalt + '" data-filetype="' + files[i].type + '" data-name="' + files[i].name + '">'
                                    +' </div>'
                                    +'<div class="img_foot" onclick="makeFeatureImage(this)" data-value="">Make Feature</div>'
                                +'</div>';
                    

                    $('#bikeImagePreview').append(html);
                    
                                
                });
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        // console.log("request ",request);
                        // var percentComplete = evt.loaded / evt.total;
                        // var percom = Math.ceil(percentComplete * 100);
                        // // console.log(this.progressId, percom);
                        // // if(percom >50) xhr.abort();
                        // $('.fileno_' + this.progressId).find('.progress-bar').css("width", percom + "%");
                        // $('.fileno_' + this.progressId).find('.progress-bar').attr("aria-valuenow", percom);
                    }
                }, false);
                return xhr;
            },
            url: '/ab-member/add_product/bikeImage',
            type: "POST",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(res);
                var mainElm = $('#image_'+res.sl);
                bikeImgGallery.push(res.file_info[0].filename);
                mainElm.find('.close_preview').attr('data-value',res.file_info[0].filename);
                mainElm.find('.img_foot').attr('data-value',res.file_info[0].filename);
                console.log(485,mainElm);
                console.log(486,mainElm.find('.img_foot'));
                console.log(486,res.file_info[0].filename);

                if(bikeFeatureImg == 'img_loader.gif'){
                    mainElm.find('.img_foot').trigger('click');
                }

                var entries = formData.entries();

                for(var pair of entries )
                {
                   formData.delete( pair[0] );
                }

            },
            complete: function(e){
                console.log(e)
                // $('.uploadbtn').addClass('active').removeClass('inactive');
            },
            error: function(err) {
                console.log(err);
            }
        });
    }
}

var tempp_shop_logo = null;
var tempp_shop_cover = null;

function fileupload(files,type){
    var request = [];
        formDataTemp.length = 0;
    for (var i = 0; i < files.length; i++){
        if(count_files > 19) {
            alert("You can upload max 20 files at a time");
            return false;
        }
        var formData = new FormData();
        var entries = formData.entries();
        for(var pair of entries )
        {
           formData.delete( pair[0] );
        }
        var has_already = false;
        formDataTemp.forEach(function(vv) {
            if (vv.name == files[i].name) {
                has_already = true;
            }
        });
        if (has_already === true) continue;
        var slid = Number(moment().unix()) + i + 1;

        formDataTemp.push(files[i]);

        formData.append('uploadImage', files[i]);
        count_files++;
        
        formData.append('sl', slid);

        if (files[i].type.startsWith('image/')) {
            var imgsrc = window.URL.createObjectURL(files[i]);
        }

        var imgalt = window.URL.createObjectURL(files[i]);
        var stopthis = () => {
            this.abort();
        };

        request[slid] = $.ajax({
            xhr: function() {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("loadstart", function(et) {
                    console.log(et);
                    
                                
                });
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        
                    }
                }, false);
                return xhr;
            },
            url: '/ab-member/file_upload',
            type: "POST",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(516,res);
                if(type == 'shop_logo'){
                    tempp_shop_logo = res.file_info[0].filename;
                }else if(type == 'shop_cover'){
                    tempp_shop_cover = res.file_info[0].filename;
                }
            },
            complete: function(e){
                console.log(e)
                // $('.uploadbtn').addClass('active').removeClass('inactive');
            },
            error: function(err) {
                console.log(err);
                alert(err);
            }
        });
    }
}
function unlinkthisFile(elm){
    var imgName = $(elm).attr('data-value');
    if(bikeFeatureImg  !== imgName){
        if(pageTitle == 'add_product'){
            socket.emit('unlinkUploadsFile',{name:imgName});
        }
        $(elm).parent('.UploadableImage').remove();
        removeA(bikeImgGallery,imgName);
    }else{
        alert('Make one feature image first.');
    }
}

function makeFeatureImage(elm){
    var imgName = $(elm).attr('data-value');
    $('#bikeImagePreview').find('.img_foot').removeClass('active');
    bikeFeatureImg = imgName;
    $(elm).addClass('active');
}

function deleteBike(elm) {
    let data = {
        bike_id : $(elm).attr('data_bike_id')
    }
    Swal.fire({
        title: 'Are you sure?',
        text: "You will not be able to recover this Product!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#008000',
        cancelButtonColor: '#ed5565',
        confirmButtonText: 'Yes, delete it!',
        preConfirm: (confirm) => {
            socket.emit('deleteBike',data,function (res) {
                if(res.msg == 'success'){
                    $(elm).parents('tr').remove();
                    return true;
                }else{
                    return false;
                }
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.value) {
            Swal.fire("Deleted!", "Your Product has been deleted.", "success");
        }else{
            Swal.fire("Error!", "Something Wrong!!", "error");
        }
      });

      
}



function checkHttp(str) {
  var s = str.split(':');
  if(s[0] == 'http' || s[0] == 'https' ){
    return true;
  }else{
    return false;
  }
}


function inputValid(str) {
  var s = str.trim();

  if(s != ''){
      return true;
  }else{
      return false;
  }
}

function upShopPro(elm){
    
    var data = {
        shop_id: $(elm).attr('data-id'),
        shop_name:$('#shopPu [name="shop_name"]').val(),
        // shop_url:$('#shopPu [name="shop_url"]').val(),
        google_location:$('#shopPu [name="google_location"]').val(),
        shop_phone:$('#shopPu [name="shop_phone"]').val(),
        emi_facilities:$('#shopPu [name="emi_facilities"]').val(),
        warranty:$('#shopPu [name="warranty"]').val(),
        installment_facilities:$('#shopPu [name="installment_facilities"]').val(),
        other_service:$('#shopPu [name="other_service"]').val(),
        google_iframe:$('#shopPu [name="google_iframe"]').val(),
        about_us:$('#shopPu [name="about_us"]').val(),
        
    }
    var youtube_link = $('#shopPu [name="youtube_link"]').val().trim();
    var web_site = $('#shopPu [name="web_site"]').val().trim();
    var facebook_page = $('#shopPu [name="facebook_page"]').val().trim();

    if(inputValid(youtube_link)){
        if(checkHttp(youtube_link)){
            data.youtube_link = youtube_link;
        }else{
            data.youtube_link = 'http://'+youtube_link;
        }
    }
    if(inputValid(web_site)){
        if(checkHttp(web_site)){
            data.web_site = web_site;
        }else{
            data.web_site = 'http://'+web_site;
        }
    }

    if(inputValid(facebook_page)){
        if(checkHttp(facebook_page)){
            data.facebook_page = facebook_page;
        }else{
            data.facebook_page = 'http://'+facebook_page;
        }
    }


    if(tempp_shop_logo != null){
        data.shop_img = tempp_shop_logo;
    }

    if(tempp_shop_cover != null){
        data.shop_cover_img = tempp_shop_cover;
    }
    if(data.shop_id !== ''){

        socket.emit('upShopProfile',data,function (res) {
            if (res.msg == 'success') {
               alert('Successfully update.');
               location.reload();
            }
        });
    }

}

socket.on('new_message',function(res){
   if(res.receiver_id == $('#shopID').val() && res.msg.conversation_id == conversation_id){
       draw_msg(res.msg);
       var scrollHeight = $("#chat_container").get(0).scrollHeight;
       $("#chat_container").animate({ scrollTop: scrollHeight });
       
   }else if(res.receiver_id == $('#shopID').val()){
    toastr.success(''+res.msg.msg_body+'',"1 New Message",);
   }
   if($('#shopConvList').is(':visible')){
       var html = $('#convId'+res.msg.conversation_id)[0].outerHTML;
       $('#convId'+res.msg.conversation_id).remove();
       $('#shopConvList').prepend(html);
    }
});

socket.on('newConvBroadCast',function(res){
    console.log(res);
    if(res.owner_id == $('#shopID').val()){
        if($('#shopConvList').is(':visible')){
            draw_conv(res);
        }
        toastr.success("1 New Customer Knock Your Shop.");
    }
});

function draw_conv(data){
    var html = '<div class="chat-user" id="convId'+data.conversation_id+'" onclick="startConv(this)" data-cov="'+data.conversation_id+'" data-cus="'+data.created_by+'">'
                    +'<img class="chat-avatar" src="/images/demo_user.png" alt="">'
                    +'<div class="chat-user-name">'
                        +'<a href="#" class="customer_name_'+data.created_by+'"></a>'
                    +'</div>'
                +'</div>';
        $('#shopConvList').prepend(html);
        getAllCustomer();
}

function changeBikeCondition(elm){
    
    if($(elm).val() == 1){
        $('.ofob').hide();
    }else{
        $('.ofob').show();
    }
}

var allcustomerlist = [];

function getAllCustomer(){
    socket.emit('getAllCustomer',function(res){
        if(res.status){
            allcustomerlist = res.data;
            $.each(res.data,function(k,v){
                $('.customer_name_'+v.customer_id).text(v.customer_name);
                $('.customer_phone_'+v.customer_id).text(v.customer_phone);
                $('.customer_email_'+v.customer_id).text(v.customer_email);
            })
        }
    })
}

getAllCustomer();

var conversation_id = '';
var customer_id = '';
function startConv(elm){
    conversation_id = $(elm).attr('data-cov');
    customer_id = $(elm).attr('data-cus');
    socket.emit('getInitialMsg',{conversation_id},function(msg){
		if(msg.status){
			$('#chat_container').html('');
			$.each(msg.data,function(k,v){

				draw_msg(v);
			});
			var scrollHeight = $("#chat_container").get(0).scrollHeight;
			$("#chat_container").animate({ scrollTop: scrollHeight });
		}else{
			console.log(msg);
		}
	});
}

function sendMsg(e) {
    var msg = $(e.target).val().trim();
    var code = e.which || e.keyCode;
    if (code == 13) {
        if (msg.length > 1) {
            if(conversation_id.length > 1){

                var data = {
                    msg_body:msg,
                    sender_id:$('#shopID').val(),
                    conversation_id:conversation_id,
                    receiver_id:customer_id
            
                }
                $(e.target).val("");
                socket.emit('sendMessage',data,function(res){
                    if(res.status){
                        draw_msg(res.msg);
                        var scrollHeight = $("#chat_container").get(0).scrollHeight;
                        $("#chat_container").animate({ scrollTop: scrollHeight });
                    }
                });
            }else{
                alert('Please select conversation.');
            }
        }
    }
}

function draw_msg(msg){
    var whoisSender = '';
    var profileimage = '';
    if(msg.sender_id == customer_id){
        whoisSender = 'left';
        profileimage = '/images/demo_user.png'
    }else{
        whoisSender = 'right';
        var adminImg = $("#shop_Img").val();
        if(adminImg == "null" || adminImg == null ){

            profileimage = '/images/uploads/bike_loader.gif';
        }else{
            profileimage = '/images/uploads/'+adminImg+'';
        }
    }
    var msg_date = moment(msg.created_at).format('hh:mm A DD-MMM-YYYY');
    if(msg_date == undefined){
        msg_date = moment().format('hh:mm A DD-MMM-YYYY');
    }
    var html = '<div class="chat-message '+whoisSender+'">'
                    +'<img class="message-avatar" src="'+profileimage+'" alt="">'
                    +'<div class="message">'
                        // +'<a class="message-author" href="#"></a>'
                        +'<span class="message-date">'+msg_date+'</span>'
                        +'<span class="message-content">'+msg.msg_body+'</span>'
                    +'</div>'
                +'</div>'
    $('#chat_container').append(html);		
}


function productBikesomeUpdate(elm,type,bike_id){
    if(type == 'offer'){
        var updatestatus = (($(elm).hasClass('btn-danger'))? true:false);
        if(updatestatus){
            

            Swal.fire({
                title: 'Submit Discount Price here.',
                input: 'number',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (confirm) => {
                  var offerprice = parseInt(confirm);
                  socket.emit('productBikesomeUpdate',{type:'offer_and_discount',bike_offer:updatestatus,bike_id:bike_id,discount_price:offerprice},function(res){
                        if(res.status){
                            $(elm).removeClass('btn-danger');
                            $(elm).addClass('btn-primary');
                            $(elm).text('Active');
                            return true;
                        }else{
                            return false;
                        }
                  })
                },
                allowOutsideClick: () => !Swal.isLoading()
              }).then((result) => {
                if(result.value){
                    Swal.fire({
                        type: 'success',
                        title: 'Submit offer price successfully.',
                        showConfirmButton: true,
                        timer: 1500
                    })
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: 'something wrong.',
                        showConfirmButton: true,
                        timer: 1500
                    })
                }
              })
        }else{
            
            socket.emit('productBikesomeUpdate',{type:'offer',bike_offer:updatestatus,bike_id:bike_id},function(res){
                if(res.status){
                    $(elm).removeClass('btn-primary');
                    $(elm).addClass('btn-danger');
                    $(elm).text('Inactive');
                }
            })
        }
    }else if(type == 'status'){
        var updatestatus = (($(elm).hasClass('btn-danger'))? 1:0);
        socket.emit('productBikesomeUpdate',{type:'status',product_status:updatestatus,bike_id:bike_id},function(res){
            if(res.status){
                if(updatestatus){
                    $(elm).removeClass('btn-danger');
                    $(elm).addClass('btn-primary');
                    $(elm).text('Active');
                }else{
                    $(elm).removeClass('btn-primary');
                    $(elm).addClass('btn-danger');
                    $(elm).text('Inactive');
                }
            }
        })

    }
}