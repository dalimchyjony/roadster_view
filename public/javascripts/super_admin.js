
function updateSUstatus(elm,type){
	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var thisUserId = $(elm).attr('data-id');
	var thisUserStatus = false;

	if($(elm).hasClass('active')){
		thisUserStatus = true;
	}else{
		thisUserStatus = false;
	}

	var data = {
		user_id:thisUserId,
		status: (thisUserStatus)? false:true,
		admin_id:user_id
	}

	if(user_id !== thisUserId){
		if(user_role == 'Admin'){
			if(type == 'email'){
				socket.emit('updateSUEStatus',data,function(res){
					if(res.msg == 'success'){
						if(data.status){
							$(elm).removeClass('inactive btn-danger');
							$(elm).addClass('active btn-primary');
							$(elm).text('Active')
						}else{
							$(elm).removeClass('active btn-primary');
							$(elm).addClass('inactive btn-danger');
							$(elm).text('Inactive')
						}
					}
				})
			}else if('status'){
				socket.emit('updateSuperUserStatus',data,function(res){
					if(res.msg == 'success'){
						if(data.status){
							$(elm).removeClass('inactive btn-danger');
							$(elm).addClass('active btn-primary');
							$(elm).text('Active')
						}else{
							$(elm).removeClass('active btn-primary');
							$(elm).addClass('inactive btn-danger');
							$(elm).text('Inactive')
						}
					}
				})
			}
		} 
	}

}

function updateShop(elm,type){

	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var thisShopId = $(elm).attr('data-id');
	var thisShopStatus = false;

	if($(elm).hasClass('active')){
		thisShopStatus = true;
	}else{
		thisShopStatus = false;
	}
	var data = {
		shop_id:thisShopId,
		shop_status: (thisShopStatus)? false:true
	}

	if(user_id !== thisShopId){
		if(user_role == 'Admin'){
			if(type == 'status'){
				socket.emit('updateShopStatus',data,function(res){
					if(res.msg == 'success'){
						if(data.shop_status){
							$(elm).removeClass('inactive btn-danger');
							$(elm).addClass('active btn-primary');
							$(elm).text('Active')
						}else{
							$(elm).removeClass('active btn-primary');
							$(elm).addClass('inactive btn-danger');
							$(elm).text('Inactive')
						}
					}
				})
			}
		} 
	}
}

function deleteSUser(elm){
	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var thisUserId = $(elm).attr('data-id');

	var data = {
		user_id:thisUserId,
		admin_id:user_id
	}

	if(user_id !== thisUserId){
		if(user_role == 'Admin'){
			socket.emit('deleteSUser',data,function(res){
				if(res.msg == 'success'){
					$(elm).parents('.userTR').remove();
				}
			});
		}
	}
}

function deletePost(elm) {
	var data = {
		post_id: $(elm).attr('data-id'),
		path: $(elm).attr('data-path')
	}
	
	swal({
		title: "Are you sure?",
		text: "Your will not be able to recover this Post!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#ed5565",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: false,
		closeOnCancel: true },
		function (isConfirm) {
		if (isConfirm) {
			socket.emit('deletePost', data, function (res) {
				if(res.msg == 'success'){
					$(elm).parents('.singlePost').remove();
				}
			});
			swal("Deleted!", "Post has been deleted.", "success");
		}
	});
}

function publishStatus(type,elm) {
	var data = {
		post_id: $(elm).attr('data-id'),
		type: type
	}

	socket.emit('publishStatus',data,function(res){
		if(res.msg == 'success'){
			console.log(res);
			if (res.status) {
				$(elm).parent('.statusField').html('<button type="button" data-id="<%= v.blog_id %>" onclick="publishStatus(false,this)" class="btn btn-primary btn-xs">Active</button>');
			} else {
				$(elm).parent('.statusField').html('<button type="button" data-id="<%= v.blog_id %>" onclick="publishStatus(true,this)" class="btn btn-danger btn-xs">Inactive</button>');
			}
		}
	});
}

function submitLocation(elm,type){

	if(type == 'country'){
		var value = $('#inputCountry').val();
		if(value !== ''){
			socket.emit('addCountry',{country_name:value},function(res){
				if(res.msg == 'success'){
					$('#inputCountry').val('');
					$('.select_country').append('<option value="'+res.data.country_id+'">'+res.data.country_name+'</option>');

					toastrview({type:'Success',msg:''+res.data.country_name+' added successfully.'});
					

				}else{
					toastrview({type:'Warning',msg:''+value+' already exist.'});
				}
			});
		}else{
			toastrview({type:'Warning',msg:'Please write country name.'});
		}

	}else if(type == 'division'){
		var value = $('#inputDivision').val();
		var countryid = $('#divisionForm').find('.select_country').val();
		if(value !== ''){
			if(countryid !== ''){
				socket.emit('addDivision',{division_name:value,country_id:countryid},function(res){
					if(res.msg == 'success'){
						$('#inputDivision').val('');

						if($('#zillaForm').find('.select_country').val() == countryid){
							$('#zillaForm').find('.select_division').append('<option value="'+res.data.division_id+'">'+res.data.division_name+'</option>')
						}
						if($('#thanaForm').find('.select_country').val() == countryid){
							$('#thanaForm').find('.select_division').append('<option value="'+res.data.division_id+'">'+res.data.division_name+'</option>')
						}

						toastrview({type:'Success',msg:''+res.data.division_name+' added successfully.'});
						

					}else{
						toastrview({type:'Warning',msg:''+value+' already exist.'});
					}
				})
			}else{
				toastrview({type:'Warning',msg:'Please select country.'});
			}
		}else{
			toastrview({type:'Warning',msg:'Please write division name.'});
		}

	}else if(type == 'zilla'){
		var value = $('#inputZilla').val();
		var countryid = $('#zillaForm').find('.select_country').val();
		var divisionid= $('#zillaForm').find('.select_division').val();

		if(value !== ''){
			if(countryid !== ''){
				if(divisionid !== ''){
					socket.emit('addZilla',{zilla_name:value,country_id:countryid,division_id:divisionid},function(res){
						if(res.msg == 'success'){
							$('#inputZilla').val('');

							if($('#thanaForm').find('.select_country').val() == countryid){
								if($('#thanaForm').find('.select_division').val() == divisionid){
									$('#thanaForm').find('.select_zilla').append('<option value="'+res.data.zilla_id+'">'+res.data.zilla_name+'</option>')
								}
							}
							toastrview({type:'Success',msg:''+res.data.zilla_name+' added successfully.'});
							

						}else{
							toastrview({type:'Warning',msg:''+value+' already exist.'});
						}
					})
				}else{
					toastrview({type:'Warning',msg:'Please select division.'});
				}
			}else{
				toastrview({type:'Warning',msg:'Please select country.'});
			}
		}else{
			toastrview({type:'Warning',msg:'Please write zilla name.'});
		}

	}else if(type == 'thana'){
		var value = $('#inputThana').val();
		var countryid = $('#thanaForm').find('.select_country').val();
		var divisionid= $('#thanaForm').find('.select_division').val();
		var zillaid = $('#thanaForm').find('.select_zilla').val();

		if(value !== ''){
			if(countryid !== ''){
				if(divisionid !== ''){
					if(zillaid !== ''){
						socket.emit('addThana',{thana_name:value,country_id:countryid,division_id:divisionid,zilla_id:zillaid},function(res){
							if(res.msg == 'success'){
								$('#inputThana').val('');

								toastrview({type:'Success',msg:''+res.data.thana_name+' added successfully.'});
								

							}else{
								toastrview({type:'Warning',msg:''+value+' already exist.'});
							}
						})
					}else{
						toastrview({type:'Warning',msg:'Please select zilla.'});
					}
				}else{
					toastrview({type:'Warning',msg:'Please select division.'});
				}
			}else{
				toastrview({type:'Warning',msg:'Please select country.'});
			}
		}else{
			toastrview({type:'Warning',msg:'Please write zilla name.'});
		}
	}
}

function toastrview(data){
	if(data.type == 'Success'){
		toastr.success(data.msg,data.type,{
	        "timeOut": "2000",
	        "extendedTImeout": "0",
	        "closeButton" :true,
	        "positionClass": "toast-bottom-right",
	    });
	}else if(data.type == 'Info'){
		toastr.info(data.msg,data.type,{
	        "timeOut": "2000",
	        "extendedTImeout": "0",
	        "closeButton" :true,
	        "positionClass": "toast-bottom-right",
	    });
	}else if(data.type == 'Warning'){
		toastr.warning(data.msg,data.type,{
	        "timeOut": "2000",
	        "extendedTImeout": "0",
	        "closeButton" :true,
	        "positionClass": "toast-bottom-right",
	    });
	}else if(data.type == 'Error'){

	}
	
}

function selectCountry(elm,type){
	var value = $(elm).val();
	$('#'+type).find('.select_division').find('option').remove();
	$('#'+type).find('.select_division').append('<option value="">Select Division</option>');
	$('#'+type).find('.select_division').val('');

	if(type == 'thanaForm'){
		$('#'+type).find('.select_zilla').find('option').remove();
		$('#'+type).find('.select_zilla').append('<option value="">Select Zilla</option>');
		$('#'+type).find('.select_zilla').val('');

	}

		if(value !== ''){
			socket.emit('getDivisionFilter',{country_id:value},function(res){
				if(res.data == null){
					toastrview({type:'Info',msg:'No division found.'});
				}else{
					$.each(res.data,function(k,v){
						$('#'+type).find('.select_division').append('<option value="'+v.division_id+'">'+v.division_name+'</option>');
					})
				}
			})
		}
}

function selectDivision(elm,type){
	var value = $(elm).val();
	$('#'+type).find('.select_zilla').find('option').remove();
	$('#'+type).find('.select_zilla').append('<option value="">Select Zilla</option>');
	$('#'+type).find('.select_zilla').val('');
	if(value !== ''){
		socket.emit('getZillaFilter',{division_id:value},function(res){
			if(res.data == null){
				toastrview({type:'Info',msg:'No zilla found.'});
			}else{
				$.each(res.data,function(k,v){
					$('#'+type).find('.select_zilla').append('<option value="'+v.zilla_id+'">'+v.zilla_name+'</option>');
				})
			}
		})
	}
}

var temppImg = null;

function getTemppImg(elm) {
  var file = elm.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
  	temppImg = reader.result;
    // console.log('RESULT', reader.result)
    $('#sliderImgViewr').attr('src',temppImg);
    $('#sliderImageViewDiv').show();
  }
  reader.readAsDataURL(file);
}

var formDataTemp = [];
var count_files = 0;

function fileupload(files,type){
    var request = [];
        formDataTemp.length = 0;
    for (var i = 0; i < files.length; i++){
        if(count_files > 19) {
            alert("You can upload max 20 files at a time");
            return false;
        }
        var formData = new FormData();
        var entries = formData.entries();
        for(var pair of entries )
        {
           formData.delete( pair[0] );
        }
        var has_already = false;
        formDataTemp.forEach(function(vv) {
            if (vv.name == files[i].name) {
                has_already = true;
            }
        });
        if (has_already === true) continue;
        var slid = Number(moment().unix()) + i + 1;

        formDataTemp.push(files[i]);

        formData.append('uploadImage', files[i]);
        count_files++;
        
        formData.append('sl', slid);

        if (files[i].type.startsWith('image/')) {
            var imgsrc = window.URL.createObjectURL(files[i]);
        }

        var imgalt = window.URL.createObjectURL(files[i]);
        var stopthis = () => {
            this.abort();
        };

        request[slid] = $.ajax({
            xhr: function() {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("loadstart", function(et) {
                    console.log(et);
                    
                                
                });
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                                            
                         }
                }, false);
                return xhr;
            },
            url: '/ab-admin/file_upload',
            type: "POST",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(516,res);
			    // console.log('RESULT', reader.result)
			    temppImg = res.file_info[0].filename;
			    $('#sliderImgViewr').attr('src','/images/uploads/'+res.file_info[0].filename);
			    $('#sliderImageViewDiv').show();

            },
            complete: function(e){
                console.log(e)
                // $('.uploadbtn').addClass('active').removeClass('inactive');
            },
            error: function(err) {
                console.log(err);
                alert(err);
            }
        });
    }
}

/////////////save new Slider

var saveBusy = false ;
function saveSlider(elm){
	if(saveBusy){
		
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		saveBusy = true;
		if( temppImg !== null){
			var data = {
				slider_title:$('#sliderForm [name="slider_title"]').val(),
				slider_desc:$('#sliderForm [name="slider_desc"]').val(),
				slider_desc2:$('#sliderForm [name="slider_desc2"]').val(),
				slider_img:temppImg,
				slider_link:$('#sliderForm [name="slider_link"]').val(),
				created_by:user_id
			}
			console.log(data)
			socket.emit('addNewSlider',data,function(res){
				if(res.msg == 'success'){
					saveBusy = false;
					$('#sliderForm [name="slider_title"]').val('');
					$('#sliderForm [name="slider_desc"]').val('');
					$('#sliderForm [name="slider_desc2"]').val('');
					$('#sliderForm [name="slider_link"]').val('');
					 $('#sliderImageViewDiv').hide();
					toastrview({type:'Success',msg:'Slider Save Successfully.'});

					var html = '<tr class="rsvSlider">'
									+'<td>'+($('.rsvSlider').length + 1)+'</td>'
									+'<td>'+data.slider_title+'</td>'
									+'<td><img src="'+temppImg+'" style="width: 200px; height: 80px"></td>'
									+'<td>'+data.slider_desc+'</td>'
									+'<td>'
										+'<button type="button" class="btn btn-outline btn-danger btn-xs inactive" onclick="updateSliderStatus(this)" data-id="'+res.slider_id+'" >Inactive</button>'
										+'<button type="button"  class="btn  btn-danger btn-xs" onclick="deleteSlider(this)" data-id="'+res.slider_id+'" ><i class="fa fa-times"></i></button>'
									+'</td>'
								+'</tr>';

					$('#rsvAllSlider').append(html);
				}else{
					toastrview({type:'Warning',msg:'Error!'});
				}
			})
		}
	}
}

function saveBrand(elm){
	if(saveBusy){
		
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		saveBusy = true;
			var data = {
				brand_name:$('#brandFrom [name="brand_name"]').val(),
				brand_desc:$('#brandFrom [name="brand_desc"]').val(),
				brand_logo:temppImg,
				created_by:user_id
			}

			socket.emit('addNewBrand',data,function(res){
				if(res.msg == 'success'){
					saveBusy = false;
					$('#brandFrom [name="slider_title"]').val('');
					$('#brandFrom [name="slider_desc"]').val('');
					toastrview({type:'Success',msg:'Brand Save Successfully.'});

					var html = '<tr class="rsvBrand">'
									+'<td>'+($('.rsvBrand').length + 1)+'</td>'
									+'<td>'+data.brand_name+'</td>'
									+'<td>'+data.brand_desc+'</td>'
									+'<td><img src="'+temppImg+'" style="width: 200px; height: 80px"></td>'
									+'<td>'
										+'<button type="button" class="btn btn-outline btn-danger btn-xs inactive" onclick="updateBrandStatus(this)" data-id="'+res.brand_id+'" >Inactive</button>'
										+'<button type="button"  class="btn  btn-danger btn-xs" onclick="deleteBrand(this)" data-id="'+res.brand_id+'" ><i class="fa fa-times"></i></button>'
									+'</td>'
								+'</tr>';

					$('#rsvBrandAll').append(html);
				}else{
					toastrview({type:'Warning',msg:'Error!'});
				}
			})
	}
}

function saveBanner(){
	if(saveBusy){
		
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		saveBusy = true;
			var data = {
				banner_title:$('#banner_form [name="banner_title"]').val(),
				banner_type:$('#banner_form [name="banner_type"]').val(),
				banner_link:$('#banner_form [name="banner_link"]').val(),
				banner_descriptions:$('#banner_form [name="banner_descriptions"]').val(),
				banner_notes:$('#banner_form [name="banner_notes"]').val(),
				banner_img:temppImg,
				status:false,
				created_by:user_id
			}
			if(temppImg !== null){
				if($('#banner_form [name="banner_type"]').val() != ''){
					if($('#banner_form [name="banner_link"]').val() != ''){

						socket.emit('addNewBanner',data,function(res){
							if(res.status){
								saveBusy = false;
								$('#banner_form [name="banner_title"]').val('');
								$('#banner_form [name="banner_type"]').val('');
								$('#banner_form [name="banner_link"]').val('');
								$('#banner_form [name="banner_descriptions"]').val('');
								$('#banner_form [name="banner_notes"]').val('');
								$('#sliderImageViewDiv').hide();
								toastrview({type:'Success',msg:'Banner Save Successfully.'});
			
								var html = '<tr class="rsvBanner">'
												+'<td>'+($('.rsvBanner').length + 1)+'</td>'
												+'<td>'+data.banner_title+'</td>'
												+'<td>'+data.banner_descriptions+'</td>'
												+'<td>'+data.banner_notes+'</td>'
												+'<td><img src="/images/uploads/'+temppImg+'" style="width: 200px; height: 80px"></td>'
												+'<td>'+data.banner_link+'</td>'
												+'<td>'
													+'<button type="button" class="btn btn-outline btn-danger btn-xs inactive" onclick="updateBannerStatus(this)" data-id="'+res.data.banner_id+'" >Inactive</button>'
													+'<button type="button"  class="btn  btn-danger btn-xs" onclick="deleteBanner(this)" data-id="'+res.data.banner_id+'" ><i class="fa fa-times"></i></button>'
												+'</td>'
											+'</tr>';
			
								$('#rsvBrandAll').append(html);
							}else{
								toastrview({type:'Warning',msg:'Error!'});
							}
						})
					}else{
						toastrview({type:'Warning',msg:'Please enter  banner link'});
						saveBusy = false;
					}

				}else{
					toastrview({type:'Warning',msg:'Please enter  banner title'});
					saveBusy = false;
				}
				
			}else{
				toastrview({type:'Warning',msg:'Please add banner image.'});
				saveBusy = false;
			}
	}
}
function deleteSlider(elm){
	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var thisSliderid = $(elm).attr('data-id');

	if(user_role == 'Admin'){
		socket.emit('deleteSlider',{slider_id:thisSliderid},function(res){
			if(res.msg == 'success'){
				$(elm).parents('.rsvSlider').remove();
			}
		});
	}
}

//////////// DELETE BRAND /////////////////////

function deleteBrand(elm){
	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var thisBrandId = $(elm).attr('data-id');

	if(user_role == 'Admin'){
		socket.emit('deleteBrand',{brand_id:thisBrandId},function(res){
			if(res.msg == 'success'){
				$(elm).parents('.rsvBrand').remove();
			}
		});
	}
}

function deleteBanner(elm){
	var user_role = $('#user_role').val();
	var user_id = $('#user_id').val();
	var banner_id = $(elm).attr('data-id');

	if(user_role == 'Admin'){
		socket.emit('deleteBanner',{banner_id:banner_id},function(res){
			if(res.msg == 'success'){
				$(elm).parents('.rsvBanner').remove();
			}
		});
	}
}

//////////// UPDATE SLIDER STATUS /////////////////////

function updateSliderStatus(elm){
	var slider_id = $(elm).attr('data-id');
	if(saveBusy){
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		if(user_role == 'Admin'){
			saveBusy = true;
			if($(elm).hasClass('active')){
				socket.emit('updateSliderStatus',{slider_id:slider_id,status:false},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('active btn-primary');
						$(elm).addClass('inactive btn-danger');
						$(elm).text('Inactive');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}else{
				socket.emit('updateSliderStatus',{slider_id:slider_id,status:true},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('inactive btn-danger');
						$(elm).addClass('active btn-primary');
						$(elm).text('Active');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}
		}else{
			toastrview({type:'Warning',msg:'Hey, Who are you..?'});
		}
	}
}

//////////// UPDATE BRAND STATUS /////////////////////

function updateBrandStatus(elm){
	var brand_id = $(elm).attr('data-id');
	if(saveBusy){
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		if(user_role == 'Admin'){
			saveBusy = true;
			if($(elm).hasClass('active')){
				socket.emit('updateBrandStatus',{brand_id:brand_id,status:false},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('active btn-primary');
						$(elm).addClass('inactive btn-danger');
						$(elm).text('Inactive');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}else{
				socket.emit('updateBrandStatus',{brand_id:brand_id,status:true},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('inactive btn-danger');
						$(elm).addClass('active btn-primary');
						$(elm).text('Active');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}
		}else{
			toastrview({type:'Warning',msg:'Hey, Who are you..?'});
		}
	}
}

function updateBannerStatus(elm){
	var banner_id = $(elm).attr('data-id');
	if(saveBusy){
		toastrview({type:'Warning',msg:'Please waiting for response.'});
	}else{
		if(user_role == 'Admin'){
			saveBusy = true;
			if($(elm).hasClass('active')){
				socket.emit('updateBannerStatus',{banner_id:banner_id,status:false},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('active btn-primary');
						$(elm).addClass('inactive btn-danger');
						$(elm).text('Inactive');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}else{
				socket.emit('updateBannerStatus',{banner_id:banner_id,status:true},function(res){
					saveBusy = false;
					if(res.msg == 'success'){
						$(elm).removeClass('inactive btn-danger');
						$(elm).addClass('active btn-primary');
						$(elm).text('Active');
						toastrview({type:'Success',msg:'Update Successfully.'});
					}else{
						toastrview({type:'Warning',msg:'Try again leter'});
					}
				});
			}
		}else{
			toastrview({type:'Warning',msg:'Hey, Who are you..?'});
		}
	}
}

var allcontactmsg = [];
function getContactMsg(){
	socket.emit('getContactMsg',function(res){
		if(res.status){
			allcontactmsg = res.data;
			$('#totalContactMsg').text(allcontactmsg.length);
		}
	})
}

$(function(){
	getContactMsg();

})

var sidepanelMsgDe = (data)=>{
	console.log(data);

	 var html ='<div class="panel panel-default" style="min-width: 210px;">'
	            +'<div class="panel-heading" style="position: relative">'
	                +'<a style="position: relative;left: -8px" data-toggle="collapse" data-parent="#accordion" href="#contactMsg'+data.contact_id+'" class="float-left">'
	                    +'<img style="width: 30px;height: 30px" alt="image" class="rounded-circle" src="/backend/img/a1.jpg">'
	                +'</a>'
	                +'<h4 class="panel-title" style="overflow: hidden;width:80%;text-overflow:ellipsis;white-space:nowrap;position: relative;top: -6px;">'
	                    +'<a data-toggle="collapse" data-parent="#accordion" href="#contactMsg'+data.contact_id+'">'+data.name+'</a>'
	                +'</h4>'
	                +'<a style="position: absolute;bottom: 5px;" data-toggle="collapse" data-parent="#accordion" href="#contactMsg'+data.contact_id+'">'+data.phone+'</a>'
	            +'</div>'
	            +'<div id="contactMsg'+data.contact_id+'" class="panel-collapse collapse">'
	                +'<div style="padding:0px 8px; font-weight:bold"<h5>Name: '+data.name+'</h5></div>'
	                +'<div style="padding:0px 8px; font-weight:bold"<h5>Email: '+data.email+'</h5></div>'
	                +'<div style="padding:0px 8px; font-weight:bold"<h5>Phone: '+data.phone+'</h5></div>'
	                +'<div class="panel-body">'+data.message+'</div>'
	            +'</div>'
	        +'</div>';

    return html;


}

function toggleContactPanel(){
	if($('#sidepanel_dashboard').is(':visible')){
		$('#sidepanel_dashboard').hide();
	}else{
		$('#side-panel-title').text('Contact Messages');
		$('#sidebar-panel-body').html("");

		$.each(allcontactmsg,function(k,v){
			$('#sidebar-panel-body').append(sidepanelMsgDe(v));
		});
		$('#sidepanel_dashboard').show();
	}
}