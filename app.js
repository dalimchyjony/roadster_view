var express = require('express');
var path = require('path');
const https = require('https');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
var socketIO = require('socket.io');
var sharedsession = require("express-socket.io-session");
var _ = require('lodash');
var fs = require('file-system');
var ejs = require('ejs');
const compression = require('compression');
let oneYear = 1 * 365 * 24 * 60 * 60 * 1000;



const secret = require('./config/keys').secret;
var session = require('express-session')({
    secret: secret,
    resave: true,
    saveUninitialized: true
  });

alluserlist = [];

var socketIO = require('socket.io');
var sharedsession = require("express-socket.io-session");

//DB config
const db = require('./config/keys').mongoURI;

//Mongo DB connection
mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(()=>{
    console.log("DB connected");
  }).catch((err)=>{
    console.log("DB Error",err);
  });






var indexRouter = require('./routes/frontend');
var suDashboard = require('./routes/super-admin');
var memberDashboard = require('./routes/member');

var app = express();

// compress all responses
app.use(compression());

// Caches the static files for a year.
app.use('/', express.static(__dirname + '/public/', { maxAge: oneYear }));
app.use('/', express.static(__dirname + '/view/', { maxAge: oneYear }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(session);

// Socket.io
var io = socketIO();
app.io = io;
io.use(sharedsession(session, {
    autoSave: true
}));
io.of('/namespace').use(sharedsession(session, {
    autoSave: true
}));

require('./socket/socket.js')(io);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/ab-admin', suDashboard);
app.use('/ab-member', memberDashboard);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;

    res.render('error', {
        message: err.status,
        error: err,
        title:'Not Found',
        page:'Not Found'
    });
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            title:'500',
            page:'500'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title:'500',
        page:'500'
    });
});


module.exports = app;
