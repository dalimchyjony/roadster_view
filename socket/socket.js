var _ = require('lodash');
var moment = require('moment');
const nodemailer = require("nodemailer");
var fs = require('fs');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'info.autobazarbd@gmail.com',
      pass: 'info.autobazarbd24689900',
  }
 });

var {
    s_user_register,
    checkVerifiaction,
    adminLoginCheck,
    findOneAdmin,
    updateSUStatus,
    updateSUEStatus,
    deleteSuperUser,
    findOneCountry,
    insertOneCountry,
    findOneDivision,
    insertOneDivision,
    findOneZilla,
    insertOneZilla,
    findOneThana,
    insertOneThana,
    getDivisionByCountry,
    getZillaByDivision,
    getThanaByZilla,
    newSlider,
    updateSliderStatus,
    upShopStatus,
    deleteSlider,
    newBrand,
    deleteBrand,
    deleteBanner,
    updateBrandStatus,
    updateBannerStatus,
    getAllBrand,
    findAllDivision,
    getContactMsg,
    publishStatus,
    deletePost,
    addNewBanner
    
  } = require('../utils/super_user');

var {
    newBikeAdd,
    deleteBike,
    updateBike,
    updateShopPro,
    updateLinkGroup,
    findBike,
    findEmail_sendVerification,
    checkVerificationCode,
    changePassword,
    getAllCustomer,
    productBikesomeUpdate
  } = require('../utils/member');
var {
  findAllDataShop,
  homeBikeFilter,
  homeBikeFilterOnlyRange,
  filterShopPage,
  filterProducts,
  searchProductByName
} = require('../utils/home');

var {
  insert_contMsg,
  proBookNow,
  createNewsletter,
  createNewComment,
  getConversationId,
  getInitialMsg,
  sendMessage,
  getbikeWithPaginate,
  getofferbikeWithPaginate
} = require('../utils/frontend');

module.exports = function (io) {
  var app = require('express');
  var router = app.Router();

  io.on('connection', function (socket) {
    socket.join('1');

    socket.on('login', function (userdata) {
      socket.join('richIT-2019');
      socket.join(userdata.from);
      socket.handshake.session.userdata = userdata;
      socket.handshake.session.save();

      if (alluserlist.indexOf(userdata.from) != -1) {
        console.log(userdata.text + ' is connected into socket');
      } else {
        alluserlist.push(userdata.from);
        console.log(userdata.text + ' is connected into socket');
      }

      socket.emit('online_user_list', alluserlist); // this emit receive only who is login
      socket.broadcast.emit('new_user_notification', socket.handshake.session.userdata); // this emit receive all users except me
      console.log(38,alluserlist)
    });

    socket.on('disconnect', function () {
      console.log('disconnect*********************',socket.handshake.session.userdata);
      io.sockets.in('richIT-2019').emit('logout', { userdata: socket.handshake.session.userdata });
      if (socket.handshake.session.userdata) {
          alluserlist = alluserlist.filter(function (el) {
            return el !== socket.handshake.session.userdata.from;
          });
          delete socket.handshake.session.userdata;
          console.log(38,alluserlist)
          socket.handshake.session.save();
        }
    });

    socket.on('s_user_register',function(data,callback){
      s_user_register(data,function(res){
        if(res.msg == 'success'){
          var toEmail = '';
          if(res.data.s_user_role == '1'){
            toEmail = ['dalimchyjony@gmail.com'];
          }else{
            toEmail = res.data.s_user_email;
          }
          const mailOptions = {
            from: 'info.autobazarbd@gmail.com', // sender address
            to:toEmail, // list of receivers
            subject: 'Autobazarbd Super user Verification', // Subject line
            html: '<div>Verification Pin: <h1>'+res.data.verification_code+'</h1></div>'// plain text body
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err){
              console.log(78,err)
            }else{
              callback(res);
            }
         });
        }else{
          callback(res);
        }
        
      });
    });

    socket.on('userVerifications',function(data,callback){
      checkVerifiaction(data, function(res){
        if(res.msg == 'success'){
            callback(res);
          }else{
            callback(res);
          }
      })
    });

    socket.on('loginAdmin',function(data,callback){
      adminLoginCheck(data,function(res){
        callback(res);
      })
    });




    //for update user status

    socket.on('updateSuperUserStatus',function(data,callback){
      findOneAdmin({s_user_id:data.admin_id},function(res){
        if(res.msg == 'success'){
          if(res.data.s_user_role == 1){
            updateSUStatus({s_user_id:data.user_id,s_user_status:data.status},function(res2){
              if(res2.msg == 'success'){
                callback(res2)
              }
            })
          }
        }
      })
    });


    // for update user email status

    socket.on('updateSUEStatus',function(data,callback){
      findOneAdmin({s_user_id:data.admin_id},function(res){
        if(res.msg == 'success'){
          if(res.data.s_user_role == 1){
            updateSUEStatus({s_user_id:data.user_id,eamil_status:data.status},function(res2){
              if(res2.msg == 'success'){
                callback(res2)
              }
            })
          }
        }
      });
    });


    //for delete super user

    socket.on('deleteSUser',function(data,callback){
      findOneAdmin({s_user_id:data.admin_id},function(res){
        if(res.msg == 'success'){
          if(res.data.s_user_role == 1){
            deleteSuperUser({s_user_id:data.user_id},function(res2){
              if(res2.msg == 'success'){
                callback(res2)
              }
            })
          }
        }
      });
    });

    socket.on('deleteBike',function (data, callback) {
      deleteBike({bike_id:data.bike_id}, function (res) {
        if (res.msg == 'success') {
          callback({msg:'success'});
        }
      });
    });

    socket.on('deletePost', function (data, callback) {
      deletePost(data, function (res) {
        if (res.msg == 'success') {
          callback({msg:'success'});
        }
      });
    });

    socket.on('publishStatus', function (data, callback) {
      publishStatus(data, function (res) {
        if (res.msg == 'success') {
          callback(res);
        }
      });
    });

    // for add country 

    socket.on('addCountry',function(data,callback){
      console.log(data)
      findOneCountry({country_name:data.country_name},function(res){
        if(res.msg == 'success'){
          if(res.data == null){
            insertOneCountry({country_name:data.country_name},function(res2){
              if(res2.msg == 'success'){
                callback(res2)
              }else{
                console.log(res2)
              }
            })
          }else{
            callback({msg:"Country alreday exist ! "})
          }
        }
      })
    });

    //for add Division

    socket.on('addDivision',function(data,callback){
      findOneDivision({division_name:data.division_name,country_id:data.country_id},function(res){
        if(res.msg == 'success'){
            if(res.data == null){
              insertOneDivision({division_name:data.division_name,country_id:data.country_id},function(res2){
                if(res2.msg == 'success'){
                  callback(res2)
                }else{
                  console.log(res2)
                }
              })
            }else{
              callback({msg:"Division alreday exist ! "})
            }
          }
      })
    });


    // for add zilla

    socket.on('addZilla',function(data,callback){
      findOneZilla({zilla_name:data.zilla_name,country_id:data.country_id,division_id:data.division_id},function(res){
        if(res.msg == 'success'){
            if(res.data == null){
              insertOneZilla({zilla_name:data.zilla_name,country_id:data.country_id,division_id:data.division_id},function(res2){
                if(res2.msg == 'success'){
                  callback(res2)
                }else{
                  console.log(res2)
                }
              })
            }else{
              callback({msg:"Zilla alreday exist ! "})
            }
          }
      })
    });

    // for add thana

    socket.on('addThana',function(data,callback){
      findOneThana({thana_name:data.thana_name,country_id:data.country_id,division_id:data.division_id,zilla_id:data.zilla_id},function(res){
        if(res.msg == 'success'){
            if(res.data == null){
              insertOneThana({thana_name:data.thana_name,country_id:data.country_id,division_id:data.division_id,zilla_id:data.zilla_id},function(res2){
                if(res2.msg == 'success'){
                  callback(res2)
                }else{
                  console.log(res2)
                }
              })
            }else{
              callback({msg:"Thana alreday exist ! "})
            }
          }
      })
    });

    // get division filter by country id

    socket.on('getDivisionFilter',function(data,callback){
      getDivisionByCountry(data,function(res){
        if(res.msg == 'success'){
          callback(res);
        }else{
          console.log(res)
        }
      })
    });


    //get zilla filter by division id

    socket.on('getZillaFilter',function(data,callback){
      getZillaByDivision(data,function(res){
        if(res.msg == 'success'){
          callback(res);
        }else{
          console.log(res)
        }
      });
    })
    
    //get thana filter by division id

    socket.on('getThanaFilter',function(data,callback){
      getThanaByZilla(data,function(res){
        if(res.msg == 'success'){
          callback(res);
        }else{
          console.log(res)
        }
      });
    });


    ////add bike product

    socket.on('addBike',function(data,callback){
      newBikeAdd(data,function(res){
        if(res.msg == 'success'){
          callback(res);
        }else{
          console.log(res)
        }
      })
    })

    ///////////////// UPDATE BIKE /////////////

    socket.on('updateBike',function(data,callback){
      updateBike(data,function(res){
        if(res.msg == 'success'){
          callback(res);
        }else{
          console.log(res)
        }
      })
    })
    


      ////unlink file

      socket.on('unlinkUploadsFile',function(data){
        var filePath = './public/images/uploads/'+data.name+''; 
            fs.unlinkSync(filePath);
      });

      ///add new slider

      socket.on('addNewSlider',function(data,callback){
        console.log(data)
          newSlider(data,function(res){
            if(res.msg == 'success'){
              callback(res);
            }else{
              console.log(res)
            }
          });
      });

      //////////////// UPDATE SLIDER STATUS ///////////////

      socket.on('updateSliderStatus',function(data,callback){
        updateSliderStatus(data,function(res){
          if(res.msg == 'success'){
              callback(res);
            }else{
              console.log(res)
            }
          });
      })

      //////////////// UPDATE BRAND STATUS ///////////////

      socket.on('updateBrandStatus',function(data,callback){
        updateBrandStatus(data,function(res){
          if(res.msg == 'success'){
              callback(res);
            }else{
              console.log(res)
            }
          });
      })
      socket.on('updateBannerStatus',function(data,callback){
        updateBannerStatus(data,function(res){
          if(res.msg == 'success'){
              callback(res);
            }else{
              console.log(res)
            }
          });
      })

      ///////////////  UPDATE SHOP STATUS //////////////

      socket.on('updateShopStatus',function(data,callback){
        upShopStatus(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      });


      ///////////////////// DELETE SLIDER ////////////////////

      socket.on('deleteSlider',function(data,callback){
        deleteSlider(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      });

      socket.on('getShopAllData',function(callback){
        findAllDataShop(function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      });


      ///////////////////// ADD NEW BRAND //////////////////

      socket.on('addNewBrand',function(data,callback){
        newBrand(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      })

      /////////////////  DELETE BRAND //////////////////

      socket.on('deleteBrand',function(data,callback){
        deleteBrand(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      });

      socket.on('deleteBanner',function(data,callback){
        deleteBanner(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        });
      });


      socket.on('getAllBrand',function(callback){
        getAllBrand(function(res){
          if(res.msg == 'success'){
            callback(res);
          }else{
            console.log(res)
          }
        })
      });

      socket.on('getAllDivision',function(callback){
        findAllDivision(function(res){
          console.log(res)
          callback(res)
        })
      })

      socket.on('filterHomeBike',function(data,callback){
        homeBikeFilter(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }
        });
      });

      socket.on('filterHomeOnlyRange',function(data,callback){
        homeBikeFilterOnlyRange(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }
        })
      })
      
      
      socket.on('upShopProfile',function(data,callback){
        updateShopPro(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }
        })
      });

      socket.on('updateLinkGroup',function(data,callback){
        updateLinkGroup(data,function(res){
          if(res.msg == 'success'){
            callback(res);
          }
        });
      });

      socket.on('sendContactMsg',function(data,callback){
        insert_contMsg(data,function(res){
          if(res.status){
            callback(res);
          }
        });
      });

      socket.on('getContactMsg',function(callback){
        getContactMsg(function(res){
          if(res.status){
            callback(res);
          }
        });
      });
      
      socket.on('bookNow',function(data,callback){
        proBookNow(data,function(res){
          if(res.status){
            callback(res);
          }
        });
      });


      socket.on('createNewsletter',function(data,callback){
        createNewsletter(data,function(res){
          if(res.status){
            callback(res);
          }
        });
      });

      socket.on('getProductbyShop',function(data,callback){
        findBike(data,function(res){
          if(res.status){
            callback(res);
          }else{
            cosole.log(res);
          }
        })
      });

      socket.on('createNewComment',function(data,callback){
        createNewComment(data).then((result)=>{
          callback(result);
        }).catch((error)=>{
          console.log(error);
        })
      });
      
      socket.on('findEmail_sendVerification',function(data,callback){
        findEmail_sendVerification(data).then((result)=>{
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      socket.on('checkVerificationCode',function(data,callback){
        checkVerificationCode(data).then((result)=>{
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      socket.on('changePassword',function(data,callback){
        changePassword(data).then((result)=>{
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      socket.on('getConversationId',function(data,callback){
        getConversationId(data).then((result)=>{
          if(result.newConv != undefined){
            socket.broadcast.emit('newConvBroadCast',result.newConv);
          }
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      
      socket.on('getInitialMsg',function(data,callback){
        getInitialMsg(data).then((result)=>{
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      socket.on('sendMessage',function(data,callback){
        sendMessage(data).then((result)=>{
          if(result.status){
            socket.broadcast.emit('new_message',{msg:result.msg,receiver_id:data.receiver_id});
            // io.to(data.receiver_id).emit('new_message',{msg:result.msg,receiver_id:data.receiver_id});

          }
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
      
      socket.on('getAllCustomer',function(callback){
        getAllCustomer().then((result)=>{
          callback(result);
        }).catch((error)=>{
          callback(error);
        })
      });
     
      socket.on('addNewBanner',function(data,callback){
        addNewBanner(data,function(res){
          callback(res);
        });
      });

      socket.on('filterShopPage',function(data,callback){
        filterShopPage(data,function(res){
          callback(res);
        });
      });

      socket.on('filterProducts',function(data,callback){
        filterProducts(data,function(res){
          callback(res);
        });
      });
      socket.on('searchProductByName',function(data,callback){
        searchProductByName(data,function(res){
          callback(res);
        });
      });
      
      socket.on('productBikesomeUpdate',function(data,callback){
        productBikesomeUpdate(data,function(res){
          callback(res);
        });
      });

      socket.on('getbikeWithPaginate',function(data,callback){
        getbikeWithPaginate(data,function(res){
          callback(res);
        });
      });
      socket.on('getofferbikeWithPaginate',function(data,callback){
        getofferbikeWithPaginate(data,function(res){
          callback(res);
        });
      });





///end socket line
  });

  return router;
}
