const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const divisionSchema = new Schema({
    country_id:{
        type:String,
        required:true
    },
    division_id:{
        type:String,
        required:true
    },
    division_name:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('division',divisionSchema);