const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const countrySchema = new Schema({
    country_id:{
        type:String,
        required:true
    },
    country_name:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('country',countrySchema);