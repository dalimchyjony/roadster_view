const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const contactSchema = new Schema({
    contact_id:{
        type:String,
        required:true
    },
    customer_id:{
        type:String,
        default:null
    },
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    message:{
        type:String,
        required:true
    },
    contact_type:{
        type:String,
        required:true
    
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('contact_message',contactSchema);