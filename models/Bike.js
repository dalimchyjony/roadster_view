const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const BikeSchema = new Schema({
    bike_id:{
        type:String,
        required:true
    },
    shop_id:{
        type:String,
        required:true
    },
    country_id:{
        type:String,
        required:true
    },
    division_id:{
        type:String,
        required:true
    },
    zilla_id:{
        type:String,
        required:true
    },
    thana_id:{
        type:String,
        required:true
    },
    shop_address:{
        type:String,
        required:true
    },
    bike_name:{
        type:String,
        required:true
    },
    bike_price:{
        type:Number,
        default:0
    },
    discount_price: { 
        type: Number, 
        default:0 
    },
    bike_offer: { 
        type: Boolean, 
        default:false
    },
    bike_condition: { 
        type: Number, 
        default:1 
    },
    bike_model:{
        type:String,
        required:true
    },
    feature_img:{
        type:String,
        required:true
    },
    img_gallery:{
        type:Array,
        default:[]
    },
    currency:{
        type:String,
        required:true
    },
    quantity:{
        type:Number,
        required:true
    },
    short_desc:{
        type:String,
        default:null
    },
    long_desc:{
        type:String,
        default:null
    },
    brand_id: { type: String, default:null },
    bike_overview:{
        brand_name: { type: String, default:null },
        brand_id: { type: String, default:null },
        type: { type: String, default:null },
        starting_method: { type: String, default:null },
        cooling_system: { type: String, default:null },
        engine_oil_recommendtion: { type: String, default:null },
        top_speed: { type: String, default:null },
        standard_warranty_years: { type: String, default:null },
        standard_warranty_km: { type: String, default:null },
        model_year: { type: Number, require:true },
        is_feature: { type: Number, default:0 },
        registration_y: { type: String, default:null },
        owner_type: { type: String, default:null },
        condition: { type: String, default:null },
        reg_number: { type: String, default:null },
        youtube_link1: { type: String, default:null },
        youtube_link2: { type: String, default:null },
        youtube_link3: { type: String, default:null },
    },
    engine:{
        engine_desc: { type: String, default:null },
        maximum_power: { type: String, default:null },
        minimum_torque: { type: String, default:null },
        bore: { type: String, default:null },
        stroke: { type: String, default:null },
        compression_ratio: { type: String, default:null },
        displacement_cc: { type: String, default:null },
        ignition: { type: String, default:null },
        no_of_cylinders: { type: String, default:null },
        fuel_system: { type: String, default:null },
        fuel_type: { type: String, default:null },
        valves_percylinder: { type: String, default:null },
        mileage_kmpl: { type: String, default:null },

    },
    transmission:{
        transmission_type: { type: String, default:null },
        no_of_gears: { type: String, default:null },
        clutch_type: { type: String, default:null },

    },
    chassis_n_suspension:{
        chassis_type: { type: String, default:null },
        suspension_front: { type: String, default:null },
        suspension_rear: { type: String, default:null },

    },
    brakes:{
        front_brake_type: { type: String, default:null },
        rear_brake_type: { type: String, default:null },
        front_brake_diameter: { type: String, default:null },
        rear_brake_diameter: { type: String, default:null },
        anti_lock_braking_system_ABS: { type: String, default:null },

    },
    wheels_n_tires:{
        tire_size_front: { type: String, default:null },
        tire_size_rear: { type: String, default:null },
        tire_type: { type: String, default:null },
        wheels_size_front: { type: String, default:null },
        wheels_size_rear: { type: String, default:null },
        wheels_type: { type: String, default:null },

    },
    dimensions:{
        overall_length: { type: String, default:null },
        overall_width: { type: String, default:null },
        overall_height: { type: String, default:null },
        ground_clearance: { type: String, default:null },
        weight: { type: String, default:null },
        fuel_tank_capacity: { type: String, default:null },
        wheelbase_mm: { type: String, default:null },
        seat_height_mm: { type: String, default:null },

    },
    electricals:{
        battery_type: { type: String, default:null },
        battery_voltage: { type: String, default:null },
        head_light: { type: String, default:null },
        tail_light: { type: String, default:null },
        indicators: { type: String, default:null },
        pilot_lamps: { type: String, default:null },

    },
    features:{
        speedometer: { type: String, default:null },
        odometer: { type: String, default:null },
        rpm_meter: { type: String, default:null },
        handle_type: { type: String, default:null },
        seat_type: { type: String, default:null },
        passenger_grab_rail: { type: String, default:null },
        engine_kill_switch: { type: String, default:null },
        digital_fuel_indicators: { type: String, default:null },
        low_fuel_warning_lamp: { type: String, default:null },
        pass_switch: { type: String, default:null },
        tachometer: { type: String, default:null },

    },
    uniq_url:{
        type:String,
        default:null
    },
    created_by:{
        type:String,
        required:true
    },
    product_status:{
        type:Number,
        required:true
    },
    rsv_status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});


BikeSchema.plugin(mongoosePaginate);



module.exports = mongoose.model('bike',BikeSchema);