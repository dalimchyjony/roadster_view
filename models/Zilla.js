const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const zillaSchema = new Schema({
    country_id:{
        type:String,
        required:true
    },
    division_id:{
        type:String,
        required:true
    },
    zilla_id:{
        type:String,
        required:true
    },
    zilla_name:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('zilla',zillaSchema);