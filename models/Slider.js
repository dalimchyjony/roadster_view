const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const sliderSchema = new Schema({
    slider_id:{
        type:String,
        required:true
    },
    slider_img:{
        type:String,
        required:true
    },
    slider_title:{
        type:String,
        default:null
    },
    slider_desc:{
        type:String,
        default:null
    },
    slider_desc2:{
        type:String,
        default:null
    },
    slider_desc3:{
        type:String,
        default:null
    },
    slider_desc4:{
        type:String,
        default:null
    },
    slider_link:{
        type:String,
        default:null
    },
    status:{
        type:Boolean,
        default:false
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('slider',sliderSchema);