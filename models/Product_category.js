const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const categorySchema = new Schema({
    category_id:{
        type:String,
        required:true
    },
    category_name:{
        type:String,
        required:true
    },
    category_tbl_name:{
        type:String,
        required:true
    },
    category_img:{
        type:String,
        required:true
    },
    category_details:{
        type:String,
        default:null
    },
    category_status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('product_category',categorySchema);