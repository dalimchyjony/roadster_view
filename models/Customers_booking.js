const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const customersBookingSchema = new Schema({
    booking_id:{
        type:String,
        required:true
    },
    booking_short_id:{
        type:String,
        required:true
    },
    customer_id:{
        type:String,
        default:null
    },
    customer_name:{
        type:String,
        default:null
    },
    customer_phone:{
        type:String,
        default:null
    },
    customer_email:{
        type:String,
        default:null
    },
    product_id:{
        type:String,
        default:true
    },
    shop_id:{
        type:String,
        default:true
    },
    product_type:{
        type:String,
        default:true
    },
    status:{
        type:Boolean,
        default:false
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('customers_booking',customersBookingSchema);