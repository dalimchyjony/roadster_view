const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuidv4 = require('uuid/v4');


//create schema
const messageSchema = new Schema({
    conversation_id:{
        type:String,
        required:true
    },
    msg_id:{
        type:String,
        required:true
    },
    msg_body:{
        type:String,
        required:true
    },
    sender_id:{
        type:String,
        required:true
    },
    seen_status:{
        type:Array,
        default:[]
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('messages',messageSchema);