const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const pImgSchema = new Schema({
    img_id:{
        type:String,
        required:true
    },
    shop_id:{
        type:String,
        required:true
    },
    user_id:{
        type:String,
        required:true
    },
    img:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('product_img',pImgSchema);