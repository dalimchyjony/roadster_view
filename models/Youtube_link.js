const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const youtubeLinkSchema = new Schema({
    product_id:{
        type:String,
        required:true
    },
    link_id:{
        type:String,
        required:true
    },
    link_for:{
        type:String,
        required:true
    },
    link:{
        type:String,
        default:null
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('youtube_link',youtubeLinkSchema);