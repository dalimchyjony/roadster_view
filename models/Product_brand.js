const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const productBrandSchema = new Schema({
    brand_id:{
        type:String,
        required:true
    },
    brand_name:{
        type:String,
        required:true
    },
    brand_logo:{
        type:String,
        default:null
    },
    brand_desc:{
        type:String,
        default:null
    },
    status:{
        type:Boolean,
        default:true
    },
    created_by:{
        type:String,
        default:null
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('product_brand',productBrandSchema);