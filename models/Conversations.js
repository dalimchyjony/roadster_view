const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuidv4 = require('uuid/v4');


//create schema
const conversationSchema = new Schema({
    conversation_id:{
        type:String,
        required:true
    },
    title:{
        type:String,
        default:null
    },
    img:{
        type:String,
        default:null
    },
    participants:{
        type:Array,
        required:true
    },
    participants_admin:{
        type:Array,
        default:[]
    },
    conv_for:{
        type:String,
        required:true
    },
    owner_id:{
        type:String,
        required:true
    },
    created_by:{
        type:String,
        required:true
    },
    last_msg_at:{
        type:Date,
        default:Date.now
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('conversations',conversationSchema);