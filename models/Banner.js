const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const bannerSchema = new Schema({
    banner_id:{
        type:String,
        required:true
    },
    banner_title:{
        type:String,
        default:null
    },
    banner_type:{
        type:String,
        required:true
    },
    banner_link:{
        type:String,
        required:true
    },
    banner_img:{
        type:String,
        required:true
    },
    banner_notes:{
        type:String,
        default:null
    },
    banner_descriptions:{
        type:String,
        default:null
    },
    created_by:{
        type:String,
        default:true
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

bannerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('banner',bannerSchema);