const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const customerSchema = new Schema({
    customer_id:{
        type:String,
        require:true
    },
    customer_name:{
        type:String,
        require:true
    },
    customer_phone:{
        type:String,
        require:true
    },
    customer_email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        default:null
    },
    img:{
        type:String,
        default:null
    },
    customer_type:{
        type:String,
        default:'customer'
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('customer',customerSchema);