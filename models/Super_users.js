const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const superUserSchema = new Schema({
    s_user_id:{
        type:String,
        required:true
    },
    s_user_email:{
        type:String,
        required:true
    },
    s_user_phone:{
        type:String,
        required:true
    },
    s_user_name:{
        type:String,
        required:true
    },
    s_user_password:{
        type:String,
        required:true
    },
    s_user_role:{
        type:Number,
        required:true
    },
    s_user_img:{
        type:String,
        required:true
    },
    s_user_status:{
        type:Boolean,
        default:false
    },
    verification_code:{
        type:String,
        default:null
    },
    email_status:{
        type:Boolean,
        default:false,
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('super_users',superUserSchema);