const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const thanaSchema = new Schema({
    country_id:{
        type:String,
        required:true
    },
    division_id:{
        type:String,
        required:true
    },
    zilla_id:{
        type:String,
        required:true
    },
    thana_id:{
        type:String,
        required:true
    },
    thana_name:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }

});



module.exports = mongoose.model('thana',thanaSchema);