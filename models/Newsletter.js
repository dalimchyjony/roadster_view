const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const newsLetterSchema = new Schema({
    newsletter_id:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    name:{
        type:String,
        default:null
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('newsletter',newsLetterSchema);