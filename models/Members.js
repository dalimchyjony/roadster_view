const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//create schema
const memberSchema = new Schema({
    member_id:{
        type:String,
        required:true
    },
    shop_id:{
        type:String,
        required:true
    },
    member_name:{
        type:String,
        required:true
    },
    member_email:{
        type:String,
        required:true
    },
    member_phone:{
        type:String,
        required:true
    },
    member_password:{
        type:String,
        required:true
    },
    member_img:{
        type:String,
        required:true
    },
    member_role:{
        type:Number,
        required:true
    },
    verification_code:{
        type:String,
        default:null
    },
    email_status:{
        type:Boolean,
        default:false,
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('members',memberSchema);