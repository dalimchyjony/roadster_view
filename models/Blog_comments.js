const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const blog_comment_schema = new Schema({
    comments_id:{
        type:String,
        required:true
    },
    blog_id:{
        type:String,
        required:true
    },
    customer_id:{
        type:String,
        required:true
    },
    comments:{
        type:String,
        required:true
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

blog_comment_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('blog_comments',blog_comment_schema);