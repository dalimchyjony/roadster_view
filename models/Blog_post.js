const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const blogPostSchema = new Schema({
    blog_id:{
        type:String,
        required:true
    },
    blog_title:{
        type:String,
        required:true
    },
    blog_short_desc:{
        type:String,
        required:true
    },
    blog_long_desc:{
        type:String,
        required:true
    },
    feature_img:{
        type:String,
        default:null
    },
    uniq_url:{
        type:String,
        default:null
    },
    author_id:{
        type:String,
        required:true
    },
    author_type:{
        type:String,
        required:true
    },
    views:{
        type:Number,
        default:0
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

blogPostSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('blog_post',blogPostSchema);